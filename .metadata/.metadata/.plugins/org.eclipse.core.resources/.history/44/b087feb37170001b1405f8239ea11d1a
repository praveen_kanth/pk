package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.EmployeeService;

public class EmployeeManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		
		if(action != null && action.equals("add"))
		{
			addEmployee(request, response);
		}
		else if(action != null && action.equals("edit")) 
		{
			updateEmployee(request, response);
		}
		else if(action !=null && action.equals("delete")) 
		{
			deleteEmployee(request, response);
		}
	}

	private void addEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int employee_id = Integer.parseInt(request.getParameter("employee_id"));
		String employee_name = request.getParameter("employee_name");
		String employee_type = request.getParameter("employee_type");
		String employee_address = request.getParameter("employee_address");
		String employee_email = request.getParameter("employee_email");
		int employee_telephone = Integer.parseInt(request.getParameter("employee_telephone"));
		String employee_password = request.getParameter("employee_password");

		EmployeeService service = new EmployeeService();
		
		Employee employee = new Employee();
		employee.setEmployee_id(employee_id);
		employee.setEmployee_name(employee_name);
		employee.setEmployee_type(employee_type);
		employee.setEmployee_address(employee_address);
		employee.setEmployee_email(employee_email);
		employee.setEmployee_telephone(employee_telephone);
		employee.setEmployee_password(employee_password);
		
		try 
		{
			boolean result = service.addEmployee(employee);
			
			if(result) {
				request.setAttribute("message", "The Employee is added successfully!");
			}		
			
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("employee-details.jsp");
		rd.forward(request, response);
		
	}
	
	private void updateEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int employee_id = Integer.parseInt(request.getParameter("employee_id"));
		String employee_name = request.getParameter("employee_name");
		String employee_type = request.getParameter("employee_type");
		String employee_address = request.getParameter("employee_address");
		String employee_email = request.getParameter("employee_email");
		int employee_telephone = Integer.parseInt(request.getParameter("employee_telephone"));
		String employee_password = request.getParameter("employee_password");
		
		Employee employee = new Employee(employee_id, employee_name, employee_type, employee_address, employee_email, employee_telephone, employee_password);
		
		EmployeeService service = new EmployeeService();
		try 
		{
			boolean result = service.updateEmployee(employee);
			if(result) {
				request.setAttribute("message", "The Employee is updated successfully. Employee_id:" + employee.getEmployee_id());
			}
			else {
				request.setAttribute("message", "The Employee is failed to updated! Employee_id:" + employee.getEmployee_id());
			}
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("");
		rd.forward(request, response);
		
	}
	
	private void deleteEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int employee_id = Integer.parseInt(request.getParameter("employee_id"));
		
		EmployeeService service = new EmployeeService();
		try 
		{
			boolean result = service.deleteEmployee(employee_id);
			if(result) {
				request.setAttribute("message", "The employee is deleted successfully. Employee_id:" + employee_id);
			}
			else {
				request.setAttribute("message", "The employee is failed to deleted! Employee_id:" + employee_id);
			}
			
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		response.sendRedirect("EmployeeViewer?action=all");
		
	}	
}
