<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System - Add Products</title>
</head>
<body>
	<br/>
	<div class = "container">
		<ul class="nav nav-pills">
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=all">Product Details</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link active" href="#">Add Product</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="search-edit.jsp">Edit/Search</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" href="EmployeeViewer?action=all">Employee Details</a>
	</li>
	</ul>
	<br/>
	
	<h2>Add new product</h2>
	<br/>
	
	<p>${message}</p>
	<form action="ProductManager" method="POST">
		<input type="hidden" name="action" value="add"/>
		<div class="form-group">
			<label for="product_name">Product Name: </label>
			<input type="text" id="product_name" name="product_name" class="form-control"/>
		</div>
			
		<div class="form-group">
			<label for="product_quantity">Quantity: </label>
			<input type="number" id="product_quantity" name="product_quantity" class="form-control"/>
		</div>
			
		<div class="form-group">
			<label for="original_price">Original Price [LKR]: </label>
			<input type="number" id="original_price" name="original_price" class="form-control"/>
		</div>
			
		<div class="form-group">
			<label for="profit">Profit [LKR]: </label>
			<input type="number" id="profit" name="profit" class="form-control"/>
		</div>
		<div class="form-group">
			<label for="selling_price">Selling Price [LKR]: </label>
			<input type="number" id="selling_price" name="selling_price" class="form-control"/>
		</div>
			
			<button type="submit" class="btn btn-primary"> Add Product </button>
		</form>	
	</div>
</body>
</html>