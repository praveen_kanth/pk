<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head> 
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System</title>
</head>
<body>
	<div class="container">
	<h2>Employee Login</h2>
	<form action="/action_page.php" class="was-validated">
	  <div class="form-group">
	    <label for="employee_id">Employee ID:</label>
	    <input type="text" class="form-control" id="employee_id" placeholder="Enter username" name="employee_id" required>
	    <div class="valid-feedback">Valid.</div>
	    <div class="invalid-feedback">Please fill out this field.</div>
	  </div>
	  <div class="form-group">
	    <label for="employee_password">Password:</label>
	    <input type="password" class="form-control" id="employee_password" placeholder="Enter password" name="employee_password" required>
	    <div class="valid-feedback">Valid.</div>
	    <div class="invalid-feedback">Please fill out this field.</div>
	  </div>
	  <div class="form-group form-check">
	    <label class="form-check-label"></label>
	    <input class="form-check-input" type="checkbox" name="remember" required> I agree on the terms and conditions.
	    <div class="valid-feedback">Valid.</div>
	    <div class="invalid-feedback">Check this checkbox to continue.</div>
	  </div>

  	<a href="home.jsp" class="btn btn-info" role="button">Login</a>
	</form>
	</div>
</body>
</html>