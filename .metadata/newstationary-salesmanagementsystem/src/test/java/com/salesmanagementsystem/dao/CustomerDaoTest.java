package com.salesmanagementsystem.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.salesmanagementsystem.model.Customer;
import com.salesmanagementsystem.service.CustomerService;

public class CustomerDaoTest {
	CustomerService service = new CustomerService();
	Customer customer = new Customer();
	@Test
	public void testAddcustomer() throws ClassNotFoundException, SQLException {
		customer.setNic("1");
		customer.setName("1");
		customer.setEmail("1");
		customer.setcontact(1);
		customer.setbranchid(100);
		boolean result = service.addCustomer(customer);
		assertTrue("Success", result);
	}

	@Test
	public void testDeletecustomer() throws ClassNotFoundException, SQLException {
		boolean result = service.deletecustomer("1");
		assertTrue("Success", result);
	}

}
