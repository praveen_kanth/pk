package com.salesmanagementsystem.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.EmployeeService;

public class EmployeeDaoTest {
	EmployeeService service = new EmployeeService();

	Employee employee = new Employee();

	@Test
	public void testAddEmployee() throws ClassNotFoundException, SQLException {
		employee.setEmployee_id("1");
		employee.setEmployee_firstname("1");
		employee.setEmployee_lastname("1");
		employee.setEmployee_type("1");
		employee.setEmployee_address("1");
		employee.setEmployee_email("1");
		employee.setEmployee_telephone(1);
		employee.setEmployee_password("1");
		employee.setPermission("1");
		employee.setbranch_id(100);
		boolean result = service.addEmployee(employee);
		assertTrue("Success", result);
	}

	@Test
	public void testDeleteEmployee() throws ClassNotFoundException, SQLException {
		boolean result = service.deleteEmployee("1");
		assertTrue("Success", result);
	}

}
