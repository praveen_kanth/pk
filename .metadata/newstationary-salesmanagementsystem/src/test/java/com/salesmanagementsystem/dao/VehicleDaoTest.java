package com.salesmanagementsystem.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.salesmanagementsystem.model.Drivers;
import com.salesmanagementsystem.model.Vehicle;
import com.salesmanagementsystem.service.VehicleService;

public class VehicleDaoTest {
	VehicleService service = new VehicleService();
	Vehicle vehicle = new Vehicle();
	Drivers drivers = new Drivers();

	@Test
	public void testAdddriver() throws ClassNotFoundException, SQLException {
		drivers.setdriverid("1");
		drivers.setname("1");
		drivers.setaddress("1");
		drivers.setcontact(1);
		boolean result = service.addDriver(drivers);
		assertTrue("Success", result);

	}

	@Test
	public void testAddvehicle() throws ClassNotFoundException, SQLException {
		vehicle.setvehicleno("1");
		vehicle.setbranchid(100);
		vehicle.setdriverid("1");

		boolean result = service.addVehicle(vehicle);
		assertTrue("Success", result);
	}

	@Test
	public void testDeletevehicle() throws ClassNotFoundException, SQLException {
		boolean result = service.deleteVehicle("1");
		assertTrue("Success", result);
	}

}
