package com.salesmanagementsystem.model;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeNoException;

import java.sql.SQLException;

import org.junit.Test;

import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.ProductService;

public class ProductTest {
	Product product = new Product();
	ProductService service = new ProductService();
	@Test
	public void testProduct() throws ClassNotFoundException, SQLException {
		
		product.setProduct_name("test");
		product.setProduct_quantity(1);
		product.setOriginal_price(1);
		product.setProfit(1);
		product.setSelling_price(1);
		product.setProduct_id(1);
		product.setbranch_id(100);
		boolean result = service.addProduct(product);
		assertTrue("branch is not exist", result);
	}
	
	@Test
	public void testDelete() throws ClassNotFoundException, SQLException {
		try{//fail("Not yet implemented");
		
	    boolean result = service.deleteProduct(1);
		assertTrue("Delete already", result);
		
		}
		
		catch (Exception t)
		{
			assumeNoException("cannot", t);
		}
	}


}
