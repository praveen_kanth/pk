package com.salesmanagementsystem.model;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeNoException;

import java.sql.SQLException;

import org.junit.Test;

import com.salesmanagementsystem.service.BranchService;

public class BranchTest {
	Branch branch = new Branch();
	@Test
	public void testBranch() throws ClassNotFoundException, SQLException {
		//fail("Not yet implemented");
		BranchService service = new BranchService();		
		
		branch.setbranch_id(100);
		branch.setbranch_name("Test");
		branch.setemail("Test");
		boolean result = service.addBranch(branch);
		assertTrue("Success", result);
	}
	
	@Test
	public void testDelete() throws ClassNotFoundException, SQLException {
		try{//fail("Not yet implemented");
		
		BranchService service = new BranchService();		
		boolean result = service.deletebranch(100);
		assertTrue("Delete already", result);
		
		}
		
		catch (Exception t)
		{
			assumeNoException("cannot", t);
		}
	}

}
