<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Add Products</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">


   function changeFunc( ) {
	   var e = document.getElementById("message").value;
	   alert( e);
   }

  </script>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">
			<li class="nav-item"><a class="nav-link active" href="#">Add
					Branch</a></li>
			<li class="nav-item"><a class="nav-link"
				href="branch_search-edit.jsp">Edit/Search</a></li>
			<li class="nav-item"><a class="nav-link"
				href="BranchViewer?action=all">Branch Details</a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>

		</ul>
		<br />

		<h2>Add new Branch</h2>
		<br />

		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<form action="BranchManager" method="POST">
			<input type="hidden" name="action" value="add" />
			<div class="form-group">
				<label for="branch_id">Branch Id: </label> <input type="text"
					id="branch_id" name="branch_id" class="form-control" />
			</div>

			<div class="form-group">
				<label for="branch_name">Name: </label> <input type="text"
					id="branch_name" name="branch_name" class="form-control" />
			</div>
			
			<div class="form-group">
				<label for="email">Email: </label> <input type="text"
					id="email" name="email" class="form-control" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>
			</div>

			<button type="submit" class="btn btn-primary">Add Branch</button>
		</form>


	</div>
</body>
</html>