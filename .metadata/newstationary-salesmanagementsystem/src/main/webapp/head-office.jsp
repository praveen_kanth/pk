<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
<!DOCTYPE html>
<html lang="en">
<head> 
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System</title>
</head>
<body>
		<div class="container">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		  
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="EmployeeViewer?action=all">Employee</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Add Branch</a>
		      </li>
		       <li class="nav-item">
		        <a class="nav-link" href="#">Delete Branch</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Add Product</a>
		      </li>
		       <li class="nav-item">
		        <a class="nav-link" href="#">Delete Product</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Vehicle</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Reports</a>
		      </li>
		       <li class="nav-item">
		        <a class="nav-link" href="home.jsp">Home</a>
		      </li>   
		    </ul>
		   </div>
		</nav>
	<br/>
	<hr class="d-sm-none">
	<div class="col-sm-8">
	<br/>

		</div>
		</div>
</body>
</html>
