<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System - Add Products</title>
</head>
<body>
	<br/>
	<div class = "container">
		<ul class="nav nav-pills">
		<li class="nav-item">
	    <a class="nav-link active" href="#">Generate Sales Report</a>
	  </li> 
	  <li class="nav-item">
	    <a class="nav-link" href="home.jsp">Home</a>
	  </li>
	  
	</ul>
	<br/>
	
	<h1 align="center">Generate Reports</h1>
	<br/>
	
	<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
	<form action="ReportManager" method="POST">		
		<div class="form-group">
			<label for="date">Date </label>
			<input type="date" id="date" name="date" class="form-control"/>
		</div>
					
		<button type="submit" class="btn btn-primary" name="action" value="addSale" onclick="this.form.action='ReportManager';"  > Generate Sales Report </button>
		<br>
		<br>
		<div class="form-group">
			<label for="date">Date </label>
			<input type="date" id="date" name="date" class="form-control"/>
		</div>
					
		<button type="submit" class="btn btn-primary" name="action" value="addPurchase" onclick="this.form.action='ReportManager';"  > Generate Purchase Report </button>
		</form>	
	</div>
</body>
</html>