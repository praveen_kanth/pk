<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">
			<li class="nav-item"><a class="nav-link" href="add-vehicle.jsp">Add
					Vehicle</a></li>
			<li class="nav-item"><a class="nav-link"
				href="vehicle_search-edit.jsp">Edit/Search</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Vehicles</a>
			</li>
			<li class="nav-item"><a class="nav-link" href="add-driver.jsp">Add
					Driver</a></li>

			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>

		<br /> <br />
		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
					<th>Vehicle No</th>
					<th>Branch Id</th>
					<th>Driver Id</th>
					<th>Delete</th>
				</tr>
			</thead>

			<tbody>
				<tag:forEach var="vehicle" items="${vehicleList}">
					<tr>
						<td>${vehicle.vehicleno}</td>
						<td>${vehicle.branchid}</td>
						<td>${vehicle.driverid}</td>
						<td>
							<form action="VehicleManager" method="POST">
								<input type="hidden" name="action" value="delete"> <input
									type="hidden" name="vehicleno" value="${vehicle.vehicleno}" />
								<button type="submit" class="btn btn-danger">Delete</button>
							</form>
						</td>
					</tr>
				</tag:forEach>
			</tbody>

		</table>
	</div>

</body>
</html>