<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Add Products</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">

			<li class="nav-item"><a class="nav-link active" href="#">Add
					Vehicle</a></li>
			<li class="nav-item"><a class="nav-link"
				href="vehicle_search-edit.jsp">Edit/Search</a></li>
			<li class="nav-item"><a class="nav-link"
				href="VehicleViewer?action=all">Vehicles</a></li>
			<li class="nav-item"><a class="nav-link" href="add-driver.jsp">Add
					Driver</a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>
		<br />

		<h2>Add New Vehicle</h2>
		<br />

		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<form action="VehicleManager" method="POST">
			<div class="form-group">
				<label for="vehicleno">Vehicle No: </label> <input type="text"
					id="vehicleno" name="vehicleno" class="form-control" />
			</div>

			<div class="form-group">
				<label for="branchid">Branch Name: </label> &nbsp; &nbsp;&nbsp; <select
					name="branchid" class="btn btn-primary" id="branchid">
					<tag:forEach items="${branchList}" var="category">
						<option value="${category.branch_id}">
							${category.branch_name}</option>
					</tag:forEach>
				</select> <br /> <br />
			</div>

			<div class="form-group">
				<label for="driverid">Driver Name: </label> &nbsp;
				&nbsp;&nbsp;&nbsp; <select name="driverid" class="btn btn-primary"
					id="driverid">
					<tag:forEach items="${driverList}" var="category">
						<option value="${category.driverid}">${category.name}</option>
					</tag:forEach>
				</select> <br /> <br />
			</div>

			<button type="submit" class="btn btn-primary" name="action"
				value="add" onclick="this.form.action='VehicleManager';">
				Save</button>
			<button type="submit" class="btn btn-primary" name="action1"
				value="veh" onclick="this.form.action='VehicleManager';">
				Add Vehicle</button>

		</form>
	</div>
</body>
</html>