<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Edit Branch</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">
			<li class="nav-item"><a class="nav-link" href="add-vehicle.jsp">Add
					Vehicle</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Edit/Search</a>
			</li>
			<li class="nav-item"><a class="nav-link "
				href="VehicleViewer?action=all">Vehicles</a></li>
			<li class="nav-item"><a class="nav-link" href="add-driver.jsp">Add
					Driver</a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>

		<br />
		<h2>Search and Edit Vehicle Details</h2>
		<br />
		<script type="text/javascript">
			var test = "${message}";
			if (test != "") {
				alert(test);
			}
		</script>
		<form action="VehicleViewer">
			<div class="form-group">
				<label for="vehicleno"> Vehicle No:</label> <input type="text"
					id="vehicleno" name="vehicleno" class="form-control"
					placeholder="Enter vehicle code to Search and Update the vehicle" />
			</div>
			<button type="submit" class="btn btn-info">Search Vehicle</button>
		</form>

		<hr />
		<form action="VehicleManager" method="POST">
			<input type="hidden" name="action" value="edit" />
			<div class="form-group">
				<label for="vehicleno">Vehicle No:</label> <input type="text"
					id="vehicleno" readonly="readonly" class="form-control"
					name="vehicleno" value="${vehicle.vehicleno}" />
			</div>

			<div class="form-group">
				<label for="branchid">Branch Id:</label> <input type="number"
					id="branchid" class="form-control" name="branchid"
					value="${vehicle.branchid}" />
			</div>

			<div class="form-group">
				<label for="driverid">Driver Id:</label> <input type="text"
					id="driverid" class="form-control" name="driverid"
					value="${vehicle.driverid}" />
			</div>


			<button type="submit" class="btn btn-warning">Update Vehicle</button>
		</form>

	</div>
</body>
</html>