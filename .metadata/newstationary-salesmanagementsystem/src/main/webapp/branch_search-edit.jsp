<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Edit Branch</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">
			<li class="nav-item"><a class="nav-link " href="add-branch.jsp">Add
					Branch</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Edit/Search</a>
			</li>
			<li class="nav-item"><a class="nav-link"
				href="BranchViewer?action=all">Branch Details</a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>

		<br />
		<h2>Search and Edit Branch Details</h2>
		<br />
		<script type="text/javascript">
			var test = "${message}";
			if (test != "") {
				alert(test);
			}
		</script>
		<form action="BranchViewer">
			<div class="form-group">
				<label for="branch_id"> Branch ID:</label> <input type="number"
					id="branch_id" name="branch_id" class="form-control"
					placeholder="Enter branch code to Search and Update the branch" />
			</div>
			<button type="submit" class="btn btn-info">Search Branch</button>
		</form>

		<hr />
		<form action="BranchManager" method="POST">
			<input type="hidden" name="action" value="edit" />
			<div class="form-group">
				<label for="branch_id">Branch ID:</label> <input type="number"
					id="branch_id" readonly="readonly" class="form-control"
					name="branch_id" value="${branch.branch_id}" />
			</div>

			<div class="form-group">
				<label for="branch_name">Branch Name:</label> <input type="text"
					id="branch_name" class="form-control" name="branch_name"
					value="${branch.branch_name}" />
			</div>


			<button type="submit" class="btn btn-warning">Update Branch</button>
		</form>

	</div>
</body>
</html>