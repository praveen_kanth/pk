<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Edit Branch</title>
</head>
<body>
	<br/>
	<div class="container">
		<ul class="nav nav-pills">
	  <li class="nav-item">
	   <a class="nav-link" href="add-employee.jsp">Add Employee</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link active" href="#">Edit/Search</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" href="EmployeeViewer?action=all">Employee Details</a>
	</li>
	<li class="nav-item">
	     <a class="nav-link" href="home.jsp">Home</a>
	  </li>
	</ul>
		
	<br/>
	<h2>Search and Edit Employee Details</h2>
	<br/>
	<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
	<form action="EmployeeViewer" >
		<div class="form-group">
			<label for="employee_id"> Employee ID:</label>
			<input type="text" id="employee_id" name="employee_id" class="form-control" placeholder="Enter employee code to Search and Update the employee"/>
		</div>
		<button type="submit" class="btn btn-info"> Search Employee</button>
	</form>
	
	<hr/>
	<form action="EmployeeManager" method="POST">
	<input type="hidden" name="action" value="edit"/>
	<div class="form-group">
		<label for="employee_id">Employee ID:</label>
		<input type="text" id="employee_id" readonly="readonly" class="form-control" name="employee_id" value="${employee.employee_id}"/>
	</div>
	
	<div class="form-group">
		<label for="employee_firstname">First Name:</label>
		<input type="text" id="employee_firstname" class="form-control" name="employee_firstname" value="${employee.employee_firstname}"/>
	</div>
	<div class="form-group">
		<label for="employee_lastname">Second Name:</label>
		<input type="text" id="employee_lastname" class="form-control" name="employee_lastname" value="${employee.employee_lastname}"/>
	</div>
	
	<div class="form-group">
		<label for="employee_type">Employee Type:</label>
		<input type="text" id="employee_type" class="form-control" name="employee_type" value="${employee.employee_type}"/>
	</div>

	
	
	<div class="form-group">
		<label for="employee_address">Employee Address</label>
		<input type="text" id="employee_address" class="form-control" name="employee_address" value="${employee.employee_address}"/>
	</div>
	
	
	<div class="form-group">
		<label for="employee_email">Employee Email:</label>
		<input type="text" id="employee_email" class="form-control" name="employee_email" value="${employee.employee_email}"/>
	</div>
	
	
	<div class="form-group">
		<label for="employee_telephone">Employee Telephone:</label>
		<input type="number" id="employee_telephone" class="form-control" name="employee_telephone" value="${employee.employee_telephone}"/>
	</div>

	<button type="submit" class="btn btn-warning"> Update Employee</button>
	</form>
	
	</div>
</body>
</html>