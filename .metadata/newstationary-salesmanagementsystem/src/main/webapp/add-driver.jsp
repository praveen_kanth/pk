<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Add Products</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">

			<li class="nav-item"><a class="nav-link" href="add-vehicle.jsp">Add
					Vehicle</a></li>
			<li class="nav-item"><a class="nav-link"
				href="vehicle_search-edit.jsp">Edit/Search</a></li>
			<li class="nav-item"><a class="nav-link"
				href="VehicleViewer?action=all">Vehicles</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Add
					Driver</a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>
		<br />

		<h2>Add new Driver</h2>
		<br />

		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<form action="VehicleManager" method="POST">
			<div class="form-group">
				<label for="driverid">Driver Id: </label> <input type="text"
					id="driverid" name="driverid" class="form-control" />
			</div>
			<div class="form-group">
				<label for="name">Driver Name: </label> <input type="text" id="name"
					name="name" class="form-control" />
			</div>
			<div class="form-group">
				<label for="address">Address: </label> <input type="text"
					id="address" name="address" class="form-control" />
			</div>
			<div class="form-group">
				<label for="contact">Contact: </label> <input type="number"
					id="contact" name="contact" class="form-control" />
			</div>

			<button type="submit" class="btn btn-primary" name="action"
				value="adddriver" onclick="this.form.action='VehicleManager';">
				Add Driver</button>
		</form>
	</div>
</body>
</html>