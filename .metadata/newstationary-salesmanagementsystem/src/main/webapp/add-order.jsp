<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Add Return</title>
<script type="text/javascript">


   function changeFunc(value) {
    var selectBox = value
    var e = document.getElementById("productid").value;
    var e = document.getElementById("productid");
    var strUser = e.options[e.selectedIndex].text;
    var thenum = strUser.replace( /^\D+/g, '');
    document.getElementById('price').value = thenum;
   

   }
   function getQuantity() {
	    var selectBox = document.getElementById("quantity").value;
	    var selectBox1 = document.getElementById('price').value;

	    var total= selectBox*selectBox1;
	    document.getElementById('amount').value = total  

	    var key = UUID.randomUUID().toString();

	   }

  </script>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">

			<li class="nav-item">
			<a class="nav-link active" href="#">
			Create Order
			</a></li>
			<li class="nav-item">
			<a class="nav-link" href="add-invoice.jsp">
			Create Invoice
			</a></li>
			<li class="nav-item">
			<a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>
		<br />

		<h2>Create Order</h2>
		<br />
		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<form action="OrderManager" method="POST">


			<div class="form-group">
				<label for="orderid">Order No: </label> 
				<input type="number"
					id="orderid" name="orderid" class="form-control" value="${order}" />
			</div>

			<div class="form-group">
				<label for="nic">Customer: </label> &nbsp; &nbsp; <select name="nic"
					class="btn btn-primary" id="nic">
					<tag:forEach items="${customerList}" var="category">
						<option value="${category.nic}">${category.name}</option>
					</tag:forEach>
				</select> <label for="branchid">Branch: </label> &nbsp; &nbsp;&nbsp; <select
					name="branchid" class="btn btn-primary" id="branchid">
					<tag:forEach items="${branchList}" var="category">
						<option value="${category.branch_id}">
							${category.branch_name}</option>
					</tag:forEach>
				</select> <label for="productid">Product: </label> &nbsp; &nbsp; <select
					name="productid" class="btn btn-primary" id="productid"
					onclick="changeFunc(value)">
					<tag:forEach items="${productList}" var="category">
						<option value="${category.product_id}">
							${category.product_name} ${category.selling_price}</option>


					</tag:forEach>
				</select> <br /> <br />
			</div>

			<div class="form-group">
				<label for="orderdate">Date: </label> <input type="date"
					id="orderdate" name="orderdate" class="form-control" />
			</div>


			<div class="form-group">
				<label for="quantity">Quantity: </label> <input type="number"
					id="quantity" name="quantity" class="form-control"
					onchange="getQuantity()" />
			</div>

			<div class="form-group">
				<label for="name">Reference: </label> <input type="text" id="name"
					name="name" class="form-control" />
			</div>

			<div class="form-group">
				<label for="price">Price: </label> <input type="number" id="price"
					name="price" class="form-control"  readonly="readonly"    value=$thenum />
			</div>

			<div class="form-group">
				<label for="amount">Amount: </label> <input type="number"
					id="amount" name="amount" class="form-control" />
			</div>

			<button type="submit" class="btn btn-primary" name="action"
				value="add" onclick="this.form.action='OrderManager';">
				Save Order</button>
			<button type="submit" class="btn btn-primary" name="action1"
				value="d" onclick="this.form.action='OrderManager';">
				Add Order</button>
		</form>
	</div>


</body>
</html>