<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">
			<li class="nav-item"><a class="nav-link" href="add-customer.jsp">Add
					Customer</a></li>
			<li class="nav-item"><a class="nav-link"
				href="customer_search-edit.jsp">Edit/Search</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Customer
					Details</a></li>


			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>

		<br />
		<h2>Stationary Customer Details</h2>
		<br />
		<p>${message}</p>
		<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
					<th>NIC</th>
					<th>Name</th>
					<th>Email</th>
					<th>contact</th>
					<th>BranchId</th>
					<th>Delete</th>
				</tr>
			</thead>

			<tbody>

				<tag:forEach var="customers" items="${customerList}">
					<tr>

						<td>${customers.nic}</td>
						<td>${customers.name}</td>
						<td>${customers.email}</td>
						<td>${customers.contact}</td>
						<td>${customers.branchid}</td>
						<td>
							<form action="CustomerManager" method="POST">
								<input type="hidden" name="action" value="delete"> <input
									type="hidden" name="nic" value="${customers.nic}" />
								<button type="submit" class="btn btn-danger">Delete</button>
							</form>
						</td>
					</tr>
				</tag:forEach>
			</tbody>

		</table>
	</div>

</body>
</html>