<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System</title>
<script>

	 function changeFunc() {

		 var response =   alert("Are you sure you want to delete it?")

		   if (response){
			      //do yes task
			    }else{
			     //do no task
			    }

		   }
	</script>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">
			<li class="nav-item"><a class="nav-link" href="add-branch.jsp">Add
					Branch</a></li>
			<li class="nav-item"><a class="nav-link"
				href="branch_search-edit.jsp">Edit/Search</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Branch
					Details</a></li>


			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>

		<br /> <br />
		<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
					<th>Branch ID</th>
					<th>Branch Name</th>
					<th>Email</th>
					<th>Delete</th>
				</tr>
			</thead>

			<tbody>
				<tag:forEach var="branch" items="${branchList}">
					<tr>
						<td>${branch.branch_id}</td>
						<td>${branch.branch_name}</td>
						<td>${branch.email}</td>
						<td>
							<form action="BranchManager" method="POST">
								<input type="hidden" name="action" value="delete"> <input
									type="hidden" name="branch_id" value="${branch.branch_id}" />
								<button type="submit" class="btn btn-danger"
									onclick="changeFunc();">Delete</button>
							</form>
						</td>
					</tr>
				</tag:forEach>
			</tbody>

		</table>
	</div>

</body>
</html>