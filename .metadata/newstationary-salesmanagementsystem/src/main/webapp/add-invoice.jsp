<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Add Return</title>
<script type="text/javascript">

function changeFunc(value) {
	var str = value;
	var e = document.getElementById("order_id");
    var strUser = e.options[e.selectedIndex].text;
    var thenum = strUser.replace("Amount", "");
    const slug = strUser.split('=').pop(); // 2020
    var i= parseInt(slug, 10);
   // const slug1 = strUser.split('=')[0]; // 2020
    //e.options[e.selectedIndex].text=slug1;
    document.getElementById('amount').value = i;
   // alert(slug);
}

  </script>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">

			<li class="nav-item"><a class="nav-link " href="add-order.jsp">
					Create Order </a></li>
			<li class="nav-item"><a class="nav-link active" href="#">
					Create Invoice </a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>
		<br />

		<h2>Create Invoice</h2>
		<br />
		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<form action="InvoiceManager" method="POST">


			<div class="form-group">
				<label for="invoiceid">Invoice No: </label> <input type="number"
					id="invoiceid" name="invoiceid" class="form-control"
					value="${invoice}" readonly="readonly" />
			</div>
			<div class="form-group">
				<label for="order_id">Order No: </label> &nbsp; &nbsp;&nbsp; <select
					name="order_id" class="btn btn-primary" id="order_id"
					onclick="changeFunc(value)">
					<tag:forEach items="${orderList}" var="category">

						<option value="${category.orderid}">${category.orderid} =
							${category.amount}</option>

					</tag:forEach>

				</select> <br /> <br />
			</div>

			<div class="form-group">
				<label for="date">invoice Date: </label> <input type="date"
					id="date" name="date" class="form-control" />
			</div>

			<div class="form-group">
				<label for="amount">Amount: </label> <input type="number"
					id="amount" name="amount" class="form-control"
					value="${orderAmount}" />
			</div>

			<div class="form-group">
				<label for="employee_id">Employee: </label> <input type="number"
					id="employee_id" name="employee_id" class="form-control"
					value="${employee}" />
			</div>

			<button type="submit" class="btn btn-primary" name="action"
				value="add" onclick="this.form.action='InvoiceManager';">
				Create Invoice</button>
			<button type="submit" class="btn btn-primary" name="action1"
				value="def" onclick="this.form.action='InvoiceManager';">
				Add Invoice</button>
		</form>
	</div>


</body>
</html>