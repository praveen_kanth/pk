<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html lang="en">
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System</title>
</head>
<body>

	<div class="jumbotron text-center" style="margin-bottom: 0">
		<h1>The JK Company</h1>
		<p>Welcome to our page to get done all your requirements</p>
	</div>

	<div class="container" style="margin-top: 30px">
		<div class="row">
			<div class="col-sm-4">
				<ul class="nav nav-pills flex-column">
					<li class="nav-item"><a class="nav-link active" href="#">Home</a>
					</li>
					<!-- <li class="nav-item">
          <a class="nav-link" href="head-office.jsp">#</a>
        </li> -->
					<li class="nav-item"><a class="nav-link" href="add-branch.jsp">Branch</a>
					</li>
					<li class="nav-item"><a class="nav-link"
						href="add-product.jsp">Product</a></li>
					<li class="nav-item"><a class="nav-link"
						href="add-employee.jsp">Employees</a></li>
					<li class="nav-item"><a class="nav-link"
						href="add-customer.jsp">Customer</a></li>
					<li class="nav-item"><a class="nav-link" href="add-order.jsp">Order</a>
					</li>
					<li class="nav-item"><a class="nav-link"
						href="add-vehicle.jsp">Vehicle</a></li>
					<li class="nav-item"><a class="nav-link"
						href="add-reports.jsp">Reports</a></li>
					<li class="nav-item"><a class="nav-link" href="index.jsp">Logout</a>
					</li>
				</ul>

				<hr class="d-sm-none">
			</div>
			<div class="col-sm-8">
				<h2>Happy Day</h2>
				<h5>Lets achieve the target</h5>
				<p>We are experiencing difficulties in delivering to certain
					areas due to mobility restrictions imposed by health authorities in
					controlling the Covid-19. As a result, deliveries may inevitably be
					delayed and we regret any frustration caused.</p>
				<br>

			</div>
		</div>
	</div>

	<div class="jumbotron text-center" style="margin-bottom: 0">
		<p>copyrights by MalarIT</p>
	</div>

</body>
</html>