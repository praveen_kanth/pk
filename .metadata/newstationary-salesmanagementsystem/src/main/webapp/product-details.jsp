<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head> 
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System</title>
	<script>

	 function changeFunc() {

		 var response =   alert("Are you sure you want to delete it?")

		   if (response){
			      //do yes task
			    }else{
			     //do no task
			    }

		   }
	</script>
</head>
<body>
	<br/>
	<div class="container">
		<ul class="nav nav-pills">
	 
	  <li class="nav-item">
	    <a class="nav-link" href="add-product.jsp">Add</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="search-edit.jsp">Edit/Search</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link active" href="#">Products</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link" href="add-stockrequest.jsp">Request Stock</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockrequest">Request Approvals</a>
	  </li>
	    
	   <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreceived">Received Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="add-stockreturn.jsp">Return Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreturn">Return Details</a>
	  </li>
	  <li class="nav-item">
		 <a class="nav-link" href="home.jsp">Home</a>
	</li>
	</ul>
		
	<br/>
	<br/>
	<p>${message}</p>
	<table class="table table-striped">
		<thead class="thead-dark">
			<tr>
				<th>Product ID</th>	
				<th>Product Name</th>
				<th>Quantity</th>
				<th>Original Price</th>	
				<th>Selling Price</th>
				<th>Branch</th>
				<th>Delete</th>	
			</tr>
		</thead>
		
		<tbody>
			<tag:forEach var = "product" items = "${productList}">
				<tr>
					<td>${product.product_id}</td>
					<td>${product.product_name}</td>
					<td>${product.product_quantity}</td>
					<td>${product.original_price}</td>					
					<td>${product.selling_price}</td>
					<td>${product.branch_id}</td>
					<td>
						<form action="ProductManager" method="POST">
							<input type="hidden" name="action" value="delete">
							<input type="hidden" name="product_id" value="${product.product_id}"/>
							<button type="submit" class="btn btn-danger" onclick="changeFunc();">Delete</button>
						</form>
					</td>
				</tr>
			</tag:forEach>
		</tbody>
		
		</table>
		</div>
		
</body>
</html>