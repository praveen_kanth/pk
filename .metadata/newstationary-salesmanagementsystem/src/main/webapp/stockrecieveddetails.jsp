<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head> 
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System</title>
</head>
<body>
	<br/>
	<div class="container">
		<ul class="nav nav-pills">
	 
	  <li class="nav-item">
	    <a class="nav-link" href="add-product.jsp">Add</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="search-edit.jsp">Edit/Search</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link " href="ProductViewer?action=all">Products</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link" href="add-stockrequest.jsp">Request Stock</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockrequest">Request Approvals</a>
	  </li>
	    
	  <li class="nav-item">
	    <a class="nav-link active" href="#">Received Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="add-stockreturn.jsp">Return Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreturn">Return Details</a>
	  </li>
	  <li class="nav-item">
		 <a class="nav-link" href="home.jsp">Home</a>
	</li>
	</ul>
		
	<br/>
	<h2>Stationary Product Details</h2>
	<br/>
	<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
	<table class="table table-striped">
		<thead class="thead-dark">
			<tr>
				<th>Request ID</th>	
				<th>Product ID</th>	
				<th>Product quantity</th>
				<th>Branch deliver</th>
				<th>Branch dispatch </th>
				<th>Received Product</th>
				
			</tr>
		</thead>
		
		<tbody>
			<tag:forEach var = "product" items = "${stockreceivedList}">
				<tr>
					<td>${product.requestid}</td>
					<td>${product.productid}</td>
					<td>${product.productquantity}</td>
					<td>${product.branch_id}</td>
					<td>${product.frombranchid}</td>
					
					<td>
						<form action="ProductManager" method="POST">
							<input type="hidden" name="action" value="receive">
							<input type="hidden" name="productid" value="${product.productid}"/>
							<input type="hidden" name="requestid" value="${product.requestid}"/>
							<input type="hidden" name="productquantity" value="${product.productquantity}"/>
							<input type="hidden" name="branch_id" value="${product.branch_id}"/>
							<input type="hidden" name="frombranchid" value="${product.frombranchid}"/>					
							<button type="submit" class="btn btn-danger">Receive</button>
						</form>
					</td>
				</tr>
			</tag:forEach>
		</tbody>
		
		</table>
		</div>
		
</body>
</html>