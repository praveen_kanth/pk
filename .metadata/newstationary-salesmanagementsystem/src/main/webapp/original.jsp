<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head> 
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System</title>

<style type="text/css" media="screen"><!--
body 
    {
    color: white;
    background-color:#C0C0C0;
    margin: 0px
    }

#horizon        
    {
    color: black;
    background-color: transparent;
    text-align: center;
    position: absolute;
    top: 20%;
    left: 0px;
    width: 100%;
    height: 1px;
    overflow: visible;
    visibility: visible;
    display: block
    }

#footer 
    {
    font-size: 11px;
    font-family: Verdana, Geneva, Arial, sans-serif;
    text-align: center;
    position: absolute;
    bottom: 80px;
    left: 0px;
    width: 100%;
    height: 20px;
    visibility: visible;
    display: block
    }
    
h1 {
  text-align: center;
   background-color: transparent;
   
   color: black;
}

--></style>
</head>
<body>
<h1 > Stationary Sales Management System </h1>
	<div id="horizon">
	<h2>Login</h2>
	<form action="EmployeeManager" method="POST">
	<input type="hidden" name="action" value="login"/>
	  <div class="form-group">
	    <input type="text" id="employee_id" placeholder="Enter username" name="employee_id" size="50" required>
	    <div class="valid-feedback">Valid.</div>
	    <div class="invalid-feedback">Please fill out this field.</div>
	  </div>
	  <div class="form-group">	   
	    <input draggable="false" type="password" id="employee_password" placeholder="Enter password" name="employee_password"size="50"required>
	    <div class="valid-feedback">Valid.</div>
	    <div class="invalid-feedback">Please fill out this field.</div>
	  </div>	 
	   <br>${message}
            <br><br> 
            
  	
  	<button type="submit" class="btn btn-primary"> login </button>
	</form>
	</div>
	<div id="footer">
	</div>
</body>
</html>