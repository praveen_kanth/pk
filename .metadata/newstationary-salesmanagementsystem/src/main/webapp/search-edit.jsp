<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Edit Products</title>
</head>
<body>
	<br/>
	<div class="container">
		<ul class="nav nav-pills">
		 <li class="nav-item">
	    <a class="nav-link" href="add-product.jsp">Add</a>
	  </li>
	    <li class="nav-item">
	    <a class="nav-link active" href="#">Edit/Search</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=all">Products</a>
	  </li>
	    <li class="nav-item">
	    <a class="nav-link" href="add-stockrequest.jsp">Request Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockrequest">Request Approvals</a>
	  </li>
	   
	<li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreceived">Received Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="add-stockreturn.jsp">Return Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreturn">Return Details</a>
	  </li>
	  <li class="nav-item">
		 <a class="nav-link" href="home.jsp">Home</a>
	</li>
	</ul>
		
	<br/>
	<h2>Search and Edit Product Details</h2>
	<br/>
<script type="text/javascript">
var test = "${message}";
    if(test != "")
        {
        alert(test);
        }
</script>
	<form action="ProductViewer" >
		<div class="form-group">
			<label for="product_id"> Product ID:</label>
			<input type="number" id="product_id" name="product_id" class="form-control" placeholder="Enter product code to Search and Update the product"/>
		</div>
		<button type="submit" class="btn btn-info"> Search Product</button>
	</form>
	
	<hr/>
	<form action="ProductManager" method="POST">
	<input type="hidden" name="action" value="edit"/>
	<div class="form-group">
		<label for="product_id">Product ID:</label>
		<input type="number" id="product_id" readonly="readonly" class="form-control" name="product_id" value="${product.product_id}"/>
	</div>
	
	<div class="form-group">
		<label for="product_name">Product Name:</label>
		<input type="text" id="product_name" class="form-control" name="product_name" value="${product.product_name}"/>
	</div>
	
	<div class="form-group">
		<label for="product_quantity">Product Quantity:</label>
		<input type="number" id="product_quantity" class="form-control" name="product_quantity" value="${product.product_quantity}"/>
	</div>
	
	<div class="form-group">
		<label for="original_price">Original Price</label>
		<input type="number" id="original_price" class="form-control" name="original_price" value="${product.original_price}"/>
	</div>

	
	<div class="form-group">
		
		<input type="number" id="profit" class="form-control" name="profit" value="${product.profit}" hidden="true"/>
	</div>
	
	<div class="form-group">
		<label for="selling_price">Selling Price</label>
		<input type="number" id="selling_price" class="form-control" name="selling_price" value="${product.selling_price}"/>
	</div>	
	<button type="submit" class="btn btn-warning"> Update Product</button>
	</form>
	
	</div>
</body>
</html>