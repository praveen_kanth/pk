<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System - Add Employees</title>
</head>
<body>
		<br/>
	<div class = "container">
		<ul class="nav nav-pills">
	 
	  <li class="nav-item">
	    <a class="nav-link active" href="#">Add Employee</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="emp_search-edit.jsp">Edit/Search</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link" href="EmployeeViewer?action=all">Employee Details</a>
	  </li>
	  
	  <li class="nav-item">
		  <a class="nav-link" href="home.jsp">Home</a>
	</li>
	</ul>
	<br/>

	<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
	<form action="EmployeeManager" method="POST">
					
		<div class="form-group">
			<label for="employee_id">Employee ID: </label>
			<input type="text" id="employee_id" name="employee_id" class="form-control"/>
		</div>	

		<div class="form-group">
			<label for="employee_firstname">First Name: </label>
			<input type="text" id="employee_firstname" name="employee_firstname" class="form-control"/>
		</div>	
							
		<div class="form-group">
			<label for="employee_lastname">Last Name: </label>
			<input type="text" id="employee_lastname" name="employee_lastname" class="form-control"/>
		</div>
				
		<div class="form-group">
			<label for="employee_type">Employee Type: </label>
			<input type="text" id="employee_type" name="employee_type" class="form-control"/>
		</div>
				
		<div class="form-group">
			<label for="employee_address">Employee Address: </label>
			<input type="text" id="employee_address" name="employee_address" class="form-control"/>
		</div>

		<div class="form-group">
			<label for="employee_email">Email: </label>
			<input type="text" id="employee_email" name="employee_email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>
		</div>
						
		<div class="form-group">
			<label for="employee_telephone">Employee Telephone: </label>
			<input type="number" id="employee_telephone" name="employee_telephone" class="form-control"/>
		</div>
									
		<div class="form-group">
			<label for="employee_password">Password: </label>
			<input type="password" id="employee_password" name="employee_password" class="form-control"/>
		</div>
		
		<div class="form-group">
			<label for="permission">Permission: </label>
			<!-- <input type="text" id="permission" name="permission" class="form-control"/> -->
			<select name="permission"class="btn btn-primary" id="permission">
			<option value=super>Super</option>
			<option value=limited>Limited</option>
			</select>
		
		
		 <label for="branch_id">Branch: </label> &nbsp; &nbsp;&nbsp; <select
					name="branch_id" class="btn btn-primary" id="branch_id">
					<tag:forEach items="${branchList}" var="category">
						<option value="${category.branch_id}">
							${category.branch_name}</option>
					</tag:forEach>
				</select>
		</div>		
			<br>
			<br>
				
		<button type="submit" class="btn btn-primary" name="action"
				value="add" onclick="this.form.action='EmployeeManager';">
				Save</button>
				
			<button type="submit" class="btn btn-primary" name="action"
				value="get" onclick="this.form.action='EmployeeManager';">
				Add Employees</button>
				
		</form>
		</div>
		

</body>
</html>