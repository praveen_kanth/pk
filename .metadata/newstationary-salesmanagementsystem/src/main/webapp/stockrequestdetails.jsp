<%@page import="javafx.scene.control.Alert"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>
<%
   String i = "1111";
	
%>	
<!DOCTYPE html>
<html>
<head> 
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System</title>
</head>
<body>
	<br/>
	<div class="container">
		<ul class="nav nav-pills">
	 
	  <li class="nav-item">
	    <a class="nav-link" href="add-product.jsp">Add</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="search-edit.jsp">Edit/Search</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link " href="ProductViewer?action=all">Products</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="add-stockrequest.jsp">Request Stock</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link active" href="#">Request Approvals</a>
	  </li>
	     
	 
	 <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreceived">Received Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="add-stockreturn.jsp">Return Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreturn">Return Details</a>
	  </li>
	   <li class="nav-item">
		 <a class="nav-link" href="home.jsp">Home</a>
	</li>
	</ul>
		
	<br/>
	<h2>Request Pending Details</h2>
	<br/>

   <br>
	<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
	<table class="table table-striped">
		<thead class="thead-dark">
			<tr>
				<th>Request ID</th>	
				<th>Product ID</th>	
				<th>Product Name</th>
				<th>Quantity</th>
				<th>Branch</th>	
				<th>Acknowledge</th>	
						
				<th>Approve with transport details</th>	
			</tr>
		</thead>
		
		<tbody>
			<tag:forEach var = "product" items = "${stockrequestList}">
				<tr>
					<td>${product.stockrequestid}</td>
					<td>${product.product_id}</td>
					<td>${product.product_name}</td>
					<td>${product.product_quantity}</td>
					<td>${product.branch_id}</td>
					<td>${product.acknowledge}</td>

						
						<td>
						<form action="ProductManager" method="POST">
						
						<input type="hidden" name="action" value="approve">
						<label for="branchid">Branch: </label> &nbsp;
							<select name="frombranchid" class="btn btn-primary"
									id="frombranchid"> 
									<tag:forEach items="${branchList}"  var="category" >
			<option value="${category.branch_id}">${category.branch_id}</option>
									</tag:forEach>
								</select>
								
								<label for="vehicleno">Vehicle: </label> &nbsp;
								<select name="vehicleno" class="btn btn-primary" id="vehicleno">
									<tag:forEach items="${vehicleList}" var="category">
										<option value="${category.vehicleno}">
											${category.vehicleno}</option>
									</tag:forEach>
								</select>
												  						
							
					
							<input type="hidden" name="product_id" value="${product.product_id}"/>
							<input type="hidden" name="product_quantity" value="${product.product_quantity}"/>
							<input type="hidden" name="product_name" value="${product.product_name}"/>
							<input type="hidden" name="branch_id" value="${product.branch_id}"/>
							<input type="hidden" name="stockrequestid" value="${product.stockrequestid}"/>
							<input type="hidden" name="frombranchid" value="${category.branch_id}"/>
							<input type="hidden" name="vehicleno" value="${category.vehicleno}"/>
																					
							&nbsp; &nbsp;<button type="submit" class="btn btn-danger"> Approve</button>
						</form>
					</td>
					
				</tr>
			</tag:forEach>
		</tbody>
		
		</table>
		
		</div>
		
</body>
</html>