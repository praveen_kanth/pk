<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Add Customers</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">

			<li class="nav-item"><a class="nav-link active" href="#">Add
					Customer</a></li>
			<li class="nav-item"><a class="nav-link"
				href="customer_search-edit.jsp">Edit/Search</a></li>
			<li class="nav-item"><a class="nav-link"
				href="CustomerViewer?action=all">Customer Details</a></li>

			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>
		<br />

		<h2>Add new Customer</h2>
		<br />
		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<form action="CustomerManager" method="POST">



			<div class="form-group">
				<label for="Nic">Nic: </label> <input type="text" id="Nic"
					name="Nic" class="form-control" />
			</div>

			<div class="form-group">
				<label for="Name">Name: </label> <input type="text" id="Name"
					name="Name" class="form-control" />
			</div>


			<div class="form-group">
				<label for="Email">Email: </label> <input type="text" id="Email"
					name="Email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>
			</div>

			<div class="form-group">
				<label for="contact">Contact: </label> <input type="number"
					id="contact" name="contact" class="form-control" />
			</div>


			<div class="form-group">
				<label for="branchid">Branch Id: </label> &nbsp; &nbsp; <select
					name="branchid" class="btn btn-primary" id="branchid">
					<tag:forEach items="${branchList}" var="category">
						<option value="${category.branch_id}">
							${category.branch_name}</option>
					</tag:forEach>
				</select> <br /> <br />


			</div>

			<button type="submit" class="btn btn-primary" name="action"
				value="add" onclick="this.form.action='CustomerManager';">
				Save</button>
			<button type="submit" class="btn btn-primary" name="action1"
				value="r" onclick="this.form.action='CustomerManager';">
				Add Customer</button>
		</form>
	</div>


</body>
</html>