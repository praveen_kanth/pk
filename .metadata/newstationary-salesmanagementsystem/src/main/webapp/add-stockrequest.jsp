<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
 <%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System - Add Customers</title>
</head>
<body>
		<br/>
	<div class = "container">
		<ul class="nav nav-pills">
	 
	  <li class="nav-item">
	    <a class="nav-link" href="add-product.jsp">Add</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="search-edit.jsp">Edit/Search</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=all">Products</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link active " href="#">Request Stock</a>
	  </li>
	   <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockrequest">Request Approvals</a>
	  </li>
	    
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreceived">Received Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="add-stockreturn.jsp">Return Stock</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="ProductViewer?action=stockreturn">Return Details</a>
	  </li>
	  <li class="nav-item">
		  <a class="nav-link" href="home.jsp">Home</a>
	</li>
	</ul>
	<br/>

	<h2>Add Stock Request</h2>
	<br/>
		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
	<form action="ProductManager" method="POST">
	
		<div class="form-group">
			<label for="stockrequestid">Request Id: </label>
			<input type="text" id="stockrequestid" name="stockrequestid" class="form-control" value="${order}"/>
		</div>
		
		<div class="form-group" >
		<label for="product_id">Product Id: </label> &nbsp; &nbsp;
			<select name="product_id" class="btn btn-primary" id="product_id">
           	 <tag:forEach items="${productList}" var="category">
                <option value="${category.product_id}">                  
                    ${category.product_name}
                </option>
          	 </tag:forEach>
        	</select>
		<br/>
		<br/>	
		</div>			
								
		<div class="form-group" >
		<label for="branchid">Branch Id: </label> &nbsp; &nbsp;&nbsp;
			<select name="branch_id" class="btn btn-primary" id="branch_id">
           	 <tag:forEach items="${branchList}" var="category">
                <option value="${category.branch_id}">                  
                    ${category.branch_name}
                </option>
          	 </tag:forEach>
        	</select>
		<br/>
		<br/>
		
		
		</div>
		
		<div class="form-group">
			<label for="product_quantity">Quantity: </label>
			<input type="number" id="product_quantity" name="product_quantity" class="form-control"/>
		</div>
															
		<button type="submit" class="btn btn-primary" name="action" value="addstockrequest" onclick="this.form.action='ProductManager';"  > Save </button>
		<button type="submit" class="btn btn-primary" name="action1" value="r" onclick="this.form.action='ProductManager';"  > Add Stock Request</button>
		</form>
		</div>
		

</body>
</html>