<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System - Edit Customer</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">
			<li class="nav-item"><a class="nav-link" href="add-customer.jsp">Add
					Customer</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Edit/Search</a>
			</li>
			<li class="nav-item"><a class="nav-link"
				href="CustomerViewer?action=all">Customer Details</a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>

		<br />
		<h2>Search and Edit Customer Details</h2>
		<br />
		<script type="text/javascript">
			var test = "${message}";
			if (test != "") {
				alert(test);
			}
		</script>
		<form action="CustomerViewer">
			<div class="form-group">
				<label for="nic"> Nic:</label> <input type="text" id="nic"
					name="nic" class="form-control"
					placeholder="Enter Customer code to Search and Update the Customer" />
			</div>
			<button type="submit" class="btn btn-info">Search Customer</button>
		</form>

		<hr />
		<form action="CustomerManager" method="POST">
			<input type="hidden" name="action" value="edit" />
			<div class="form-group">
				<label for="nic">Nic:</label> <input type="text" id="nic"
					readonly="readonly" class="form-control" name="nic"
					value="${customer.nic}" />
			</div>

			<div class="form-group">
				<label for="name"> Name:</label> <input type="text" id="name"
					class="form-control" name="name" value="${customer.name}" />
			</div>
			<div class="form-group">
				<label for="email">Email:</label> <input type="text" id="email"
					class="form-control" name="email" value="${customer.email}" />
			</div>

			<div class="form-group">
				<label for="contact">Contact:</label> <input type="text"
					id="contact" class="form-control" name="contact"
					value="${customer.contact}" />
			</div>


			<div class="form-group">
				<label for="branchid">Branch:</label> <input type="text"
					id="branchid" class="form-control" name="branchid"
					value="${customer.branchid}" />
			</div>


			<button type="submit" class="btn btn-warning">Update
				Customer</button>
		</form>

	</div>
</body>
</html>