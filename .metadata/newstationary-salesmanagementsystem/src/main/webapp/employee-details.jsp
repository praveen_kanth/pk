<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Insert title here</title>
</head>
<body>
	<br/>
	<div class="container">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">		  
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link active" href="#">Add Employee</a>
		      </li>		
		      <li class="nav-item">
		        <a class="nav-link" href="home.jsp">Home</a>
		      </li>      
		    </ul>
		   </div>
		</nav>
		<br/>
		
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
	  Open modal
	</button>
	
	<!-- The Modal -->
	<div class="modal" id="myModal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	
	      <!-- Modal Header -->
	      <div class="modal-header">
	        <h4 class="modal-title">Modal Heading</h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	
	      <!-- Modal body -->
	      <div class="modal-body">
	        Modal body..
	      </div>
	
	      <!-- Modal footer -->
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	      </div>
	
	    </div>
	  </div>
	</div>
	<h2>Employee Details</h2>
	<br/>
	<p>${message}</p>
	<table class="table table-striped">
		<thead class="thead-dark">
			<tr>
				<th>Employee ID</th>	
				<th>First Name</th>
				<th>Last Name</th>
				<th>Employee Type</th>
				<th>Address</th>	
				<th>Email</th>				
				<th>Telephone</th>
				<th>Permission</th>
				<th>Branch</th>
				<th>Delete</th>	
			</tr>
		</thead>
		
		<tbody>
			<tag:forEach var = "employee" items = "${employeeList}">
				<tr>
					<td>${employee.employee_id}</td>
					<td>${employee.employee_firstname}</td>
					<td>${employee.employee_lastname}</td>
					<td>${employee.employee_type}</td>
					<td>${employee.employee_address}</td>
					<td>${employee.employee_email}</td>					
					<td>${employee.employee_telephone}</td>
					<td>${employee.permission}</td>
					<td>${employee.branch_id}</td>
					<td>
						<form action="EmployeeManager" method="POST">
							<input type="hidden" name="action" value="delete">
							<input type="hidden" name="employee_id" value="${employee.employee_id}"/>
							<button type="submit" class="btn btn-danger">Delete</button>
						</form>
					</td>
				</tr>
			</tag:forEach>
		</tbody>
		
		</table>
		</div>
		
</body>
</html>