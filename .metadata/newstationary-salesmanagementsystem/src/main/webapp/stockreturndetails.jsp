<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="tag" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link
	href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Stationary Sales Management System</title>
</head>
<body>
	<br />
	<div class="container">
		<ul class="nav nav-pills">

			<li class="nav-item"><a class="nav-link" href="add-product.jsp">Add</a>
			</li>
			<li class="nav-item"><a class="nav-link" href="search-edit.jsp">Edit/Search</a>
			</li>
			<li class="nav-item"><a class="nav-link "
				href="ProductViewer?action=all">Products</a></li>
			<li class="nav-item"><a class="nav-link"
				href="add-stockrequest.jsp">Request Stock</a></li>
			<li class="nav-item"><a class="nav-link"
				href="ProductViewer?action=stockrequest">Request Approvals</a></li>

			<li class="nav-item"><a class="nav-link"
				href="ProductViewer?action=stockreceived">Received Stock</a></li>
			<li class="nav-item"><a class="nav-link"
				href="add-stockreturn.jsp">Return Stock</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Return
					Details</a></li>
			<li class="nav-item"><a class="nav-link" href="home.jsp">Home</a>
			</li>
		</ul>

		<br />
		<h2>Stationary Return Details</h2>
		<br />
		<script type="text/javascript">
   		 var test = "${message}";
    	if(test != "")
        {
        alert(test);
        }
		</script>
		<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
					<th>Return ID</th>
					<th>Product ID</th>
					<th>Product quantity</th>
					<th>Branch</th>

				</tr>
			</thead>

			<tbody>
				<tag:forEach var="product" items="${stockreturnList}">
					<tr>
						<td>${product.returnid}</td>
						<td>${product.productid}</td>
						<td>${product.productquantity}</td>
						<td>${product.branchid}</td>
					</tr>
				</tag:forEach>
			</tbody>

		</table>
	</div>

</body>
</html>