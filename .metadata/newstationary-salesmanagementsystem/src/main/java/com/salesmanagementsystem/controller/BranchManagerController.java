package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.EmployeeService;

public class BranchManagerController  extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try
		{
		String action = request.getParameter("action");			
		Employee employee1 = new Employee();
		HttpSession session = request.getSession();
		employee1=(Employee)session.getAttribute("employee");
       if(employee1.getPermission().equalsIgnoreCase("super")) {    	 
		if(action != null && action.equals("add"))
		{
			addBranch(request, response);
		}
		else if(action != null && action.equals("edit")) 
		{
			updateBranch(request, response);
		}
		else if(action !=null && action.equals("delete")) 
		{
			deleteBranch(request, response);
		}
		
       }
       else
       {  	   
    	   noAccess(request,response);
       }
		}
		catch (Exception ws)
		{			
			noAccess(request,response);
		}
	}
	
	private void noAccess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {					
		String action = request.getParameter("action");	
		request.setAttribute("message", "No Access!");			
		if(action != null && action.equals("edit"))
		{
			 	
				RequestDispatcher rd = request.getRequestDispatcher("branch_search-edit.jsp");
				rd.forward(request, response);
			
		}
		else
		{
			 		
				RequestDispatcher rd = request.getRequestDispatcher("add-branch.jsp");
				rd.forward(request, response);
		}
	   
		
	}

	private void addBranch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String branch_name = request.getParameter("branch_name");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		String email=request.getParameter("email");
		BranchService service = new BranchService();		
		Branch branch = new Branch();
		branch.setbranch_id(branch_id);
		branch.setbranch_name(branch_name);
		branch.setemail(email);
				
		try 
		{
			
			boolean result = service.addBranch(branch);
			
			if(result) {
				request.setAttribute("message", "The branch is added successfully!");
			}		
			
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("add-branch.jsp");
		rd.forward(request, response);
		
	}
	
	private void updateBranch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		String branch_name = request.getParameter("branch_name");
		
		
		Branch branch = new Branch(branch_id, branch_name);
		
		BranchService service = new BranchService();
		try 
		{
			boolean result = service.updatebranch(branch);
			if(result) {
				request.setAttribute("message", "The branch is updated successfully. branch_id:" + branch.getbranch_id());
			}
			else {
				request.setAttribute("message", "The branch is failed to updated! branch_id:" + branch.getbranch_id());
			}
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("branch_search-edit.jsp");
		rd.forward(request, response);
		
	}
	
	private void deleteBranch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		
		BranchService service = new BranchService();
		try 
		{
			boolean result = service.deletebranch(branch_id);
			if(result) {
				request.setAttribute("message", "The branch is deleted successfully. branch_id:" + branch_id);
			}
			else {
				request.setAttribute("message", "The branch is failed to deleted! branch_id:" + branch_id);
			}
			
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		response.sendRedirect("BranchViewer?action=all");
		
	}	
}
