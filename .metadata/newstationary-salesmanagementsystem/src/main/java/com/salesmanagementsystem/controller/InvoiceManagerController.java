package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salesmanagementsystem.dao.EmailClient;
import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Customer;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.model.Invoice;
import com.salesmanagementsystem.model.Order;
import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.CustomerService;
import com.salesmanagementsystem.service.InvoiceService;
import com.salesmanagementsystem.service.OrderService;
import com.salesmanagementsystem.service.ProductService;

public class InvoiceManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Employee employee1 = new Employee();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			String action1 = request.getParameter("action1");
			HttpSession session = request.getSession();
			employee1 = (Employee) session.getAttribute("employee");
			if (action != null && action.equals("add")) {
				addInvoice(request, response);
			}

			else if (action != null && action.equals("delete")) {
				deleteInvoice(request, response);
			} else if (action1 != null && action1.equals("def")) {

				CustomerService service2 = new CustomerService();
				OrderService ser = new OrderService();
				InvoiceService ser1 = new InvoiceService();
				try {

					List<Customer> customerList = service2.getcustomers();
					request.setAttribute("customerList", customerList);

					List<Order> orderList = ser.getOrders();
					request.setAttribute("orderList", orderList);
					request.setAttribute("orderAmount", orderList.get(0).getAmount());

					Invoice invoice = ser1.getInvoiceNo();

					request.setAttribute("invoice", invoice.getInvoiceid());
					request.setAttribute("employee", employee1.getEmployee_id());

				} catch (ClassNotFoundException | SQLException e) {

					request.setAttribute("message", e.getMessage());
				}

				RequestDispatcher rd = request.getRequestDispatcher("add-invoice.jsp");
				rd.forward(request, response);
			}
			else
			{
				RequestDispatcher rd = request.getRequestDispatcher("add-invoice.jsp");
				rd.forward(request, response);
			}

		}

		catch (Exception ws) {
			RequestDispatcher rd = request.getRequestDispatcher("add-invoice.jsp");
			rd.forward(request, response);
		}
	}

	private void addInvoice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int invoiceid = Integer.parseInt(request.getParameter("invoiceid"));
		int order_id = Integer.parseInt(request.getParameter("order_id"));
		String date = request.getParameter("date");
		Double amount = Double.parseDouble(request.getParameter("amount"));
		InvoiceService service = new InvoiceService();
		Invoice invoice = new Invoice();
		invoice.setInvoiceid(invoiceid);
		invoice.setOrderid(order_id);
		invoice.setDate(date);
		invoice.setAmount(amount);
		invoice.setEmployeeid(employee1.getEmployee_id());

		try {

			boolean result = service.addInvoice(invoice);

			// start
			EmailClient s = new EmailClient();
			try {

				s.sendAsHtml(employee1.getEmployee_email(), "Order Approved",
						"<h2>Thanks for the purchase on </h2>" + date + "<p> Invoice no is    " + invoiceid
								+ " <br> <br> amount is   " + amount + "<h3> from employee  "
								+ employee1.getEmployee_id() + "</h3><h4> of order ID " + order_id + "</h4></p>");
			} catch (MessagingException e) {
				// TODO Auto-generated catch
				e.printStackTrace();
			}

			// end

			if (result) {
				request.setAttribute("message", "The invoice is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-invoice.jsp");
		rd.forward(request, response);

	}

	private void deleteInvoice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int invoiceid = Integer.parseInt(request.getParameter("invoiceid"));

		InvoiceService service = new InvoiceService();
		try {
			boolean result = service.deleteInvoice(invoiceid);
			if (result) {
				request.setAttribute("message", "The invoice is deleted successfully. invoice_id:" + invoiceid);
			} else {
				request.setAttribute("message", "The invoice is failed to deleted! invoice_id:" + invoiceid);
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("InvoiceViewer?action=all");

	}

}
