package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salesmanagementsystem.dao.LogindDao;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.EmployeeService;

public class LoginManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private void loginEmployee(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String employee_id = request.getParameter("employee_id");
		String password = request.getParameter("employee_password");

		LogindDao Service = new LogindDao();
		try {
			Employee employee = Service.loginEmployee(employee_id);
			String destPage = "home.jsp";

			if (employee != null) {
				HttpSession session = request.getSession();
				session.setAttribute("employee", employee);
				destPage = "home.jsp";
			} else {
				String message = "Invalid email/password";
				request.setAttribute("message", message);
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher(destPage);
			dispatcher.forward(request, response);

		} catch (SQLException | ClassNotFoundException ex) {
			throw new ServletException(ex);
		}

	}
}
