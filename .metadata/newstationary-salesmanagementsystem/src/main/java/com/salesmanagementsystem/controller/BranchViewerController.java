package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.BranchService;

public class BranchViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			Employee employee1 = new Employee();
			HttpSession session = request.getSession();
			employee1 = (Employee) session.getAttribute("employee");
			if (employee1.getPermission().equalsIgnoreCase("super")) {
				if (action != null && action.equals("all")) {
					getBranchList(request, response);
				} else {
					getBranch(request, response);
				}
			} else {
				noAccess(request, response);
			}
		} catch (Exception d) {
			noAccess(request, response);
		}

	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("message", "No Access!");
		RequestDispatcher rd = request.getRequestDispatcher("branch_search-edit.jsp");
		rd.forward(request, response);

	}

	private void getBranchList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		BranchService service = new BranchService();

		try {

			List<Branch> branchList = service.getbranchs();
			request.setAttribute("branchList", branchList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("branch-details.jsp");
		rd.forward(request, response);

	}

	public void getBranch(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int branch_id = Integer.parseInt(request.getParameter("branch_id"));

		BranchService service = new BranchService();

		try {
			Branch branch = service.getbranch(branch_id);
			request.setAttribute("branch", branch);
		}

		catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("branch_search-edit.jsp");
		rd.forward(request, response);
	}
}
