package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.salesmanagementsystem.dao.Reports;
import com.salesmanagementsystem.model.Employee;

public class ReportManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Employee employee1 = new Employee();
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			
			HttpSession session = request.getSession();
			employee1 = (Employee) session.getAttribute("employee");
			//if (employee1.getPermission().equalsIgnoreCase("super")) {
				if (action != null && action.equals("addSale")) {
					addSale(request, response);
				} else if (action != null && action.equals("addPurchase")) {
					addPurchase(request, response);
				}

			 /*}
				 * else {
				 * 
				 * noAccess(request, response); }
				 */
		} catch (Exception ws) {
			// JOptionPane.showMessageDialog(null, "No Access", "InfoBox: " + "Info",
			// JOptionPane.INFORMATION_MESSAGE);
			noAccess(request, response);
			
			System.out.println(ws.toString());
		}
	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		request.setAttribute("message", "No Access!");
		if (action != null && action.equals("edit")) {

			RequestDispatcher rd = request.getRequestDispatcher("add-reports.jsp");
			rd.forward(request, response);

		} else {

			RequestDispatcher rd = request.getRequestDispatcher("add-reports.jsp");
			rd.forward(request, response);
		}

	}

	private void addSale(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ClassNotFoundException, SQLException {

		// testing for report
		request.setAttribute("message", "The Report is generated successfully!");
		Reports s = new Reports();
		s.sales(employee1);
		// testing for report

		RequestDispatcher rd = request.getRequestDispatcher("add-reports.jsp");
		rd.forward(request, response);

	}
	
	private void addPurchase(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ClassNotFoundException, SQLException {

		// testing for report
		request.setAttribute("message", "The Report is generated successfully!");
		Reports s = new Reports();
		s.purchase(employee1);
		// testing for report

		RequestDispatcher rd = request.getRequestDispatcher("add-reports.jsp");
		rd.forward(request, response);

	}

}
