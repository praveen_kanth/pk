package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salesmanagementsystem.model.Order;
import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Customer;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.OrderService;
import com.salesmanagementsystem.service.ProductService;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.CustomerService;

public class OrderManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			String action1 = request.getParameter("action1");

			if (action != null && action.equals("add")) {
				addOrder(request, response);
			}

			else if (action != null && action.equals("delete")) {
				deleteOrder(request, response);
			}

			else if (action1 != null && action1.equals("d")) {
				BranchService service = new BranchService();
				ProductService service1 = new ProductService();
				CustomerService service2 = new CustomerService();
				OrderService ser = new OrderService();
				try {
					Employee employee = new Employee();
					HttpSession session = request.getSession();
					employee=(Employee)session.getAttribute("employee");
					List<Branch> branchList = service.getbranchs();
					request.setAttribute("branchList", branchList);
					//all products, begin
					//List<Product> productList = service1.getProducts();
					//request.setAttribute("productList", productList);
					List<Product> productList = service1.getProductsByBranch(employee);
					request.setAttribute("productList", productList);					
					//all product,end
					
					List<Customer> customerList = service2.getcustomers();
					request.setAttribute("customerList", customerList);

					Order order = ser.getOrderNo();

					request.setAttribute("order", order.getOrderid());

				} catch (ClassNotFoundException | SQLException e) {

					request.setAttribute("message", e.getMessage());
				}

				RequestDispatcher rd = request.getRequestDispatcher("add-order.jsp");
				rd.forward(request, response);
			}
			else
			{
				RequestDispatcher rd = request.getRequestDispatcher("add-order.jsp");
				rd.forward(request, response);
			}

		} catch (Exception ws) {
			//System.out.println("error");
			request.setAttribute("message", ws.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher("add-order.jsp");
			rd.forward(request, response);
		}
	}

	private void addOrder(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name = request.getParameter("name");
		int orderid = Integer.parseInt(request.getParameter("orderid"));
		String nic = request.getParameter("nic");
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		int productid = Integer.parseInt(request.getParameter("productid"));
		int branchid = Integer.parseInt(request.getParameter("branchid"));
		String date = request.getParameter("orderdate");
		double price = Double.parseDouble(request.getParameter("price"));
		double amount = Double.parseDouble(request.getParameter("amount"));
		OrderService service = new OrderService();
		Order Order = new Order();
		Order.setOrderid(orderid);
		Order.setNic(nic);
		Order.setDate(date);
		Order.setBranchid(branchid);
		Order.setName(name);
		Order.setAmount(amount);
		Order.setProduct_id(productid);
		Order.setQuantity(quantity);
		Order.setPrice(price);

		try {

			boolean result = service.addOrder(Order);

			if (result) {
				request.setAttribute("message", "The Order is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-order.jsp");
		rd.forward(request, response);

	}

	private void deleteOrder(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int orderid = Integer.parseInt(request.getParameter("orderid"));

		OrderService service = new OrderService();
		try {
			boolean result = service.deleteOrder(orderid);
			if (result) {
				request.setAttribute("message", "The Order is deleted successfully. Order_id:" + orderid);
			} else {
				request.setAttribute("message", "The Order is failed to deleted! Order_id:" + orderid);
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("OrderViewer?action=all");

	}
}
