package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.EmployeeService;

public class EmployeeManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Employee employee1 = new Employee();
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			
			HttpSession session = request.getSession();
			employee1 = (Employee) session.getAttribute("employee");

			if (action != null && action.equals("add") && employee1.getPermission().equalsIgnoreCase("super")) {
				addEmployee(request, response);
			} else if (action != null && action.equals("edit") && employee1.getPermission().equalsIgnoreCase("super")) {
				updateEmployee(request, response);
			} else if (action != null && action.equals("delete")
					&& employee1.getPermission().equalsIgnoreCase("super")) {
				deleteEmployee(request, response);
			} else if (action != null && action.equals("login")) {

				loginEmployee(request, response);

			}

			else if (action != null && action.equals("logout")) {

				logout(request, response);

			}

			else if (action != null && action.equals("get")) {
				BranchService service = new BranchService();

				try {

					List<Branch> branchList = service.getbranchs();
					request.setAttribute("branchList", branchList);

				} catch (ClassNotFoundException | SQLException e) {

					request.setAttribute("message", e.getMessage());
				}

				RequestDispatcher rd = request.getRequestDispatcher("add-employee.jsp");
				rd.forward(request, response);
			} else {
				noAccess(request, response);
			}

		} catch (Exception a) {
			System.out.println("error");
		}
	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		request.setAttribute("message", "No Access!");
		if (action != null && action.equals("edit")) {

			RequestDispatcher rd = request.getRequestDispatcher("emp_search-edit.jsp");
			rd.forward(request, response);

		} else {
			RequestDispatcher rd = request.getRequestDispatcher("add-employee.jsp");
			rd.forward(request, response);
		}

	}

	private void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession();
		session.removeAttribute("employee");
		request.setAttribute("message", "Logout!");
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	}

	private void addEmployee(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String employee_id = request.getParameter("employee_id");
		String employee_firstname = request.getParameter("employee_firstname");
		String employee_lastname = request.getParameter("employee_lastname");
		String employee_type = request.getParameter("employee_type");
		String employee_address = request.getParameter("employee_address");
		String employee_email = request.getParameter("employee_email");
		int employee_telephone = Integer.parseInt(request.getParameter("employee_telephone"));
		String employee_password = request.getParameter("employee_password");
		String permission = request.getParameter("permission");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));

		EmployeeService service = new EmployeeService();

		Employee employee = new Employee();
		employee.setEmployee_id(employee_id);
		employee.setEmployee_firstname(employee_firstname);
		employee.setEmployee_lastname(employee_lastname);
		employee.setEmployee_type(employee_type);
		employee.setEmployee_address(employee_address);
		employee.setEmployee_email(employee_email);
		employee.setEmployee_telephone(employee_telephone);
		employee.setEmployee_password(employee_password);
		employee.setPermission(permission);
		employee.setbranch_id(branch_id);

		try {
			boolean result = service.addEmployee(employee);

			if (result) {
				request.setAttribute("message", "The Employee is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-employee.jsp");
		rd.forward(request, response);

	}

	private void updateEmployee(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String employee_id = request.getParameter("employee_id");
		String employee_firstname = request.getParameter("employee_firstname");
		String employee_lastname = request.getParameter("employee_lastname");
		String employee_type = request.getParameter("employee_type");
		String employee_address = request.getParameter("employee_address");
		String employee_email = request.getParameter("employee_email");
		int employee_telephone = Integer.parseInt(request.getParameter("employee_telephone"));
		String employee_password = request.getParameter("employee_password");
		String permission = request.getParameter("permission");
		int branch_id = employee1.getbranch_id();

		Employee employee = new Employee(employee_id, employee_firstname, employee_lastname, employee_type,
				employee_address, employee_email, employee_telephone, employee_password, permission, branch_id);

		EmployeeService service = new EmployeeService();
		try {
			boolean result = service.updateEmployee(employee);
			if (result) {
				request.setAttribute("message",
						"The Employee is updated successfully. Employee_id:" + employee.getEmployee_id());
			} else {
				request.setAttribute("message",
						"The Employee is failed to updated! Employee_id:" + employee.getEmployee_id());
			}
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("emp_search-edit.jsp");
		rd.forward(request, response);

	}

	private void deleteEmployee(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String employee_id = request.getParameter("employee_id");

		EmployeeService service = new EmployeeService();
		try {
			boolean result = service.deleteEmployee(employee_id);
			if (result) {
				request.setAttribute("message", "The employee is deleted successfully. Employee_id:" + employee_id);
			} else {
				request.setAttribute("message", "The employee is failed to deleted! Employee_id:" + employee_id);
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("EmployeeViewer?action=all");

	}

	private void loginEmployee(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String employee_id = request.getParameter("employee_id");
		String password = request.getParameter("employee_password");
		try {
			EmployeeService Service = new EmployeeService();
			Employee employee = Service.loginEmployee(employee_id, password);
			String destPage = "index.jsp";
			String message = "Invalid email/password";
			if (employee != null) {
				HttpSession session = request.getSession();

				session.setAttribute("employee", employee);
				destPage = "home.jsp";
			}

			else {

				request.setAttribute("message", message);
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher(destPage);
			dispatcher.forward(request, response);
		} catch (SQLException | ClassNotFoundException ex) {

			throw new ServletException(ex);
		}

	}
}
