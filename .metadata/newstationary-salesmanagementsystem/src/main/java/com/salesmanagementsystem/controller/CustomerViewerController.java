package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.salesmanagementsystem.model.Customer;
import com.salesmanagementsystem.service.CustomerService;

public class CustomerViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");

			if (action != null && action.equals("all")) {

				getCustomerList(request, response);
			}

			else {

				getCustomer(request, response);
			}

		} catch (Exception d) {
			noAccess(request, response);
		}

	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("message", "No Access!");
		RequestDispatcher rd = request.getRequestDispatcher("customer_search-edit.jsp");
		rd.forward(request, response);

	}

	private void getCustomerList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CustomerService service = new CustomerService();
		try {

			List<Customer> customerList = service.getcustomers();

			request.setAttribute("customerList", customerList);

		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}
		RequestDispatcher rd = request.getRequestDispatcher("customer-details.jsp");

		rd.forward(request, response);
	}

	public void getCustomer(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String Nic = request.getParameter("nic");

		CustomerService service = new CustomerService();

		try {
			Customer customer = service.getcustomer(Nic);
			request.setAttribute("customer", customer);
		}

		catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("customer_search-edit.jsp");
		rd.forward(request, response);
	}
}
