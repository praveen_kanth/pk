package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.salesmanagementsystem.dao.EmailClient;
import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.model.Order;
import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.model.StockReceived;
import com.salesmanagementsystem.model.StockRequest;
import com.salesmanagementsystem.model.StockReturn;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.ProductService;
import com.salesmanagementsystem.service.StockRequestService;
import com.salesmanagementsystem.service.StockReturnService;
import com.sun.xml.internal.fastinfoset.sax.Properties;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class ProductManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String action = request.getParameter("action");
			String action1 = request.getParameter("action1");
			// session handling
			Employee employee = new Employee();
			HttpSession session = request.getSession();
			employee = (Employee) session.getAttribute("employee");

			if (employee.getPermission().equalsIgnoreCase("super")) {

				if (action != null && action.equals("add")) {
					addProduct(request, response);
				} else if (action != null && action.equals("edit")) {
					updateProduct(request, response);
				} else if (action != null && action.equals("delete")) {
					deleteProduct(request, response);
				} else if (action1 != null && action1.equals("r")) {
					noAccess(request, response);
				} else if (action1 != null && action1.equals("g")) {
					BranchService service = new BranchService();
					ProductService service1 = new ProductService();
					try {

						List<Branch> branchList = service.getbranchs();
						request.setAttribute("branchList", branchList);
						List<Product> productList = service1.getProducts();
						request.setAttribute("productList", productList);
					} catch (ClassNotFoundException | SQLException e) {

						request.setAttribute("message", e.getMessage());
					}

					RequestDispatcher rd = request.getRequestDispatcher("add-product.jsp");
					rd.forward(request, response);
				} else if (action1 != null && action1.equals("d")) {
					noAccess(request, response);
				}

				else if (action != null && action.equals("addstockrequest")) {
					// addStockRequest(request, response);
					noAccess(request, response);
				} else if (action != null && action.equals("approve")) {

					addStockReceived(request, response);

				} else if (action != null && action.equals("receive")) {

					updateStockRequest(request, response);

				} else if (action != null && action.equals("addstockreturn")) {

					// addStockReturn(request, response);

					noAccess(request, response);
				}

			}

			else if (action != null && action.equals("addstockrequest")) {
				addStockRequest(request, response);
			} else if (action != null && action.equals("addstockreturn")) {

				addStockReturn(request, response);

			} else if (action1 != null && action1.equals("r")) {
				BranchService service = new BranchService();
				ProductService service1 = new ProductService();
				StockRequestService ser = new StockRequestService();
				try {

					List<Branch> branchList = service.getbranchs();
					request.setAttribute("branchList", branchList);
					List<Product> productList = service1.getProducts();
					request.setAttribute("productList", productList);
					StockRequest order = ser.getStockRequestNo();

					request.setAttribute("order", order.getStockrequestid());
				} catch (ClassNotFoundException | SQLException e) {

					request.setAttribute("message", e.getMessage());
				}

				RequestDispatcher rd = request.getRequestDispatcher("add-stockrequest.jsp");
				rd.forward(request, response);
			} else if (action1 != null && action1.equals("d")) {
				BranchService service = new BranchService();
				ProductService service1 = new ProductService();
				StockReturnService ser = new StockReturnService();

				try {

					List<Branch> branchList = service.getbranchs();
					request.setAttribute("branchList", branchList);
					List<Product> productList = service1.getProducts();
					request.setAttribute("productList", productList);
					StockReturn order = ser.getStockReturnNo();

					request.setAttribute("order", order.getreturnid());
				} catch (ClassNotFoundException | SQLException e) {

					request.setAttribute("message", e.getMessage());
				}

				RequestDispatcher rd = request.getRequestDispatcher("add-stockreturn.jsp");
				rd.forward(request, response);
			} else if (action != null && action.equals("approve")) {

				addStockReceived(request, response);

			}

			else if (action != null && action.equals("receive")) {

				updateStockRequest(request, response);

			}

			else {

				noAccess(request, response);
			}
		} catch (Exception a) {
			System.out.println("error" + a.toString());

		}
	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");

		if (action != null && action.equals("edit")) {
			request.setAttribute("message", "No Access!");
			RequestDispatcher rd = request.getRequestDispatcher("search-edit.jsp");
			rd.forward(request, response);

		}

		else if (action != null && action.equals("addstockrequest")) {
			request.setAttribute("message", "No Access!");
			RequestDispatcher rd = request.getRequestDispatcher("add-stockrequest.jsp");
			rd.forward(request, response);

		}

		else if (action != null && action.equals("addstockreturn")) {
			request.setAttribute("message", "No Access!");
			RequestDispatcher rd = request.getRequestDispatcher("add-stockreturn.jsp");
			rd.forward(request, response);

		} else {
			request.setAttribute("message", "No Access!");
			RequestDispatcher rd = request.getRequestDispatcher("add-product.jsp");
			rd.forward(request, response);
		}

	}

	private void addProduct(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String product_name = request.getParameter("product_name");
		int product_quantity = Integer.parseInt(request.getParameter("product_quantity"));
		double original_price = Double.parseDouble(request.getParameter("original_price"));
		double profit = Double.parseDouble(request.getParameter("profit"));
		double selling_price = Double.parseDouble(request.getParameter("selling_price"));
		int product_id = Integer.parseInt(request.getParameter("product_id"));
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		ProductService service = new ProductService();

		Product product = new Product();
		product.setProduct_name(product_name);
		product.setProduct_quantity(product_quantity);
		product.setOriginal_price(original_price);
		product.setProfit(profit);
		product.setSelling_price(selling_price);
		product.setProduct_id(product_id);
		product.setbranch_id(branch_id);
		try {
			boolean result = service.addProduct(product);

			if (result) {
				request.setAttribute("message", "The product is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-product.jsp");
		rd.forward(request, response);

	}

	private void updateProduct(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int product_id = Integer.parseInt(request.getParameter("product_id"));
		String product_name = request.getParameter("product_name");
		int product_quantity = Integer.parseInt(request.getParameter("product_quantity"));
		double original_price = Double.parseDouble(request.getParameter("original_price"));
		double profit = Double.parseDouble(request.getParameter("profit"));
		double selling_price = Double.parseDouble(request.getParameter("selling_price"));
		int branch_id = 0;
		Product product = new Product(product_id, product_name, product_quantity, original_price, profit, selling_price,
				branch_id);

		ProductService service = new ProductService();
		try {
			boolean result = service.updateProduct(product);
			if (result) {
				request.setAttribute("message",
						"The product is updated successfully. Product_id:" + product.getProduct_id());
			} else {
				request.setAttribute("message",
						"The product is failed to updated! Product_id:" + product.getProduct_id());
			}
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("search-edit.jsp");
		rd.forward(request, response);

	}

	private void deleteProduct(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int product_id = Integer.parseInt(request.getParameter("product_id"));

		ProductService service = new ProductService();
		try {
			boolean result = service.deleteProduct(product_id);
			if (result) {
				request.setAttribute("message", "The product is deleted successfully. Product_id:" + product_id);
			} else {
				request.setAttribute("message", "The product is failed to deleted! Product_id:" + product_id);
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("ProductViewer?action=all");

	}

	private void addStockRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			String stockrequestid = request.getParameter("stockrequestid");
			StockRequestService service = new StockRequestService();
			ProductService prod = new ProductService();

			int product_quantity = Integer.parseInt(request.getParameter("product_quantity"));
			int product_id = Integer.parseInt(request.getParameter("product_id"));
			int branch_id = Integer.parseInt(request.getParameter("branch_id"));
			String product_name = prod.getProduct(product_id).getProduct_name();
			String acknowledge = "Pending";

			StockRequest stockrequest = new StockRequest();
			stockrequest.setStockrequestid(stockrequestid);
			stockrequest.setProduct_name(product_name);
			stockrequest.setProduct_id(product_id);
			stockrequest.setProduct_quantity(product_quantity);
			stockrequest.setBranch_id(branch_id);
			stockrequest.setAcknowledge(acknowledge);

			boolean result = service.addStockRequest(stockrequest);

			if (result) {
				request.setAttribute("message", "The StockRequest is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-stockrequest.jsp");
		rd.forward(request, response);

	}

	private void updateStockRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int product_id = Integer.parseInt(request.getParameter("productid"));
		int product_quantity = Integer.parseInt(request.getParameter("productquantity"));
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int from_branch_id = Integer.parseInt(request.getParameter("frombranchid"));
		String requestid = request.getParameter("requestid");
		Product product = new Product();
		product.setProduct_id(product_id);
		product.setProduct_quantity(product_quantity);
		product.setbranch_id(branch_id);
		product.setFrom_branch_id(from_branch_id);
		ProductService service = new ProductService();
		StockRequestService ser = new StockRequestService();
		try {

			boolean result = service.updateProductStock(product);
			boolean result1 = ser.deleteStockReceived(requestid);
			if (result) {
				request.setAttribute("message",
						"The product is updated successfully. Product_id:" + product.getProduct_id());
			} else {
				request.setAttribute("message",
						"The product is failed to updated! Product_id:" + product.getProduct_id());
			}
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("ProductViewer?action=stockreceived");

	}

	private void addStockReceived(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String requestid = request.getParameter("stockrequestid");
		String product_name = request.getParameter("product_name");
		int product_quantity = Integer.parseInt(request.getParameter("product_quantity"));
		int product_id = Integer.parseInt(request.getParameter("product_id"));
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int frombranchid = Integer.parseInt(request.getParameter("frombranchid"));
		String vehicleno = request.getParameter("vehicleno");

		StockRequestService service = new StockRequestService();
		StockRequest stockrequest = new StockRequest();
		StockReceived stockreceived = new StockReceived();
		stockreceived.setrequestid(requestid);
		stockreceived.setproductid(product_id);
		stockreceived.setproductquantity(product_quantity);
		stockreceived.setbranch_id(branch_id);
		stockreceived.setfrombranchid(frombranchid);

		stockrequest.setStockrequestid(requestid);
		stockrequest.setProduct_name(product_name);
		stockrequest.setProduct_id(product_id);
		stockrequest.setProduct_quantity(product_quantity);
		stockrequest.setBranch_id(branch_id);
		stockrequest.setAcknowledge("Transfered");

		try {
			BranchService service1 = new BranchService();
			Branch branch = service1.getbranch(branch_id);

			// start
			EmailClient s = new EmailClient();
			try {
				s.sendAsHtml(branch.getemail(), "Order Approved",
						"<h2>Your request has been accepted ready to deliver </h2>" + "<p> product item is    "
								+ product_id + " <br> <br> quantity is   " + product_quantity + "<h3> from branch  "
								+ frombranchid + "</h3><h4> from vehicle " + vehicleno + "</h4></p>");
			} catch (MessagingException e) {
				// TODO Auto-generated catch
				e.printStackTrace();
			}

			// end

			boolean result = service.addStockReceives(stockreceived);
			boolean result1 = service.updateStockRequest(stockrequest);

			if (result) {
				request.setAttribute("message", "The StockRecieved is added successfully!");
			}

			if (result1) {
				request.setAttribute("message", "The Stock is transferred!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("ProductViewer?action=stockrequest");
	}

	private void addStockReturn(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String returnid = request.getParameter("returnid");
		int productquantity = Integer.parseInt(request.getParameter("productquantity"));
		int productid = Integer.parseInt(request.getParameter("productid"));
		int branchid = Integer.parseInt(request.getParameter("branchid"));

		StockReturnService service = new StockReturnService();

		StockReturn stockreturn = new StockReturn();
		stockreturn.setreturnid(returnid);
		stockreturn.setproductid(productid);
		stockreturn.setproductquantity(productquantity);
		stockreturn.setbranchid(branchid);

		try {
			boolean result = service.addStockReturn(stockreturn);

			if (result) {
				request.setAttribute("message", "The StockReturn is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-stockreturn.jsp");
		rd.forward(request, response);

	}

}
