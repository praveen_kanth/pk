package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.model.Vehicle;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.VehicleService;

public class VehicleViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");

			if (action != null && action.equals("all")) {
				getVehicleList(request, response);
			} else {
				getVehicle(request, response);
			}
		} catch (Exception d) {
		}

	}

	private void getVehicleList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		VehicleService service = new VehicleService();

		try {

			List<Vehicle> vehicleList = service.getVehicles();
			request.setAttribute("vehicleList", vehicleList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("vehicle-details.jsp");
		rd.forward(request, response);

	}

	public void getVehicle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String vehicleno = request.getParameter("vehicleno");

		VehicleService service = new VehicleService();

		try {
			Vehicle vehicle = service.getVehicle(vehicleno);
			request.setAttribute("vehicle", vehicle);
		}

		catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("vehicle_search-edit.jsp");
		rd.forward(request, response);
	}
}
