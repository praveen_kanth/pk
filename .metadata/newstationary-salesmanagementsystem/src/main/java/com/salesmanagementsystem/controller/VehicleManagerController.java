package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Drivers;
import com.salesmanagementsystem.model.Vehicle;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.VehicleService;

public class VehicleManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			String action1 = request.getParameter("action1");

			if (action != null && action.equals("add")) {
				addVehicle(request, response);
			} else if (action != null && action.equals("edit")) {
				updateVehicle(request, response);
			} else if (action != null && action.equals("delete")) {
				deleteVehicle(request, response);
			} else if (action1 != null && action1.equals("veh")) {

				BranchService service = new BranchService();
				VehicleService service1 = new VehicleService();

				try {

					List<Branch> branchList = service.getbranchs();
					request.setAttribute("branchList", branchList);
					List<Drivers> productList = service1.getDrivers();
					request.setAttribute("driverList", productList);
				} catch (ClassNotFoundException | SQLException e) {

					request.setAttribute("message", e.getMessage());
				}

				RequestDispatcher rd = request.getRequestDispatcher("add-vehicle.jsp");
				rd.forward(request, response);
			} else if (action != null && action.equals("adddriver")) {
				addDriver(request, response);
			}
		}

		catch (Exception ws) {

		}
	}

	private void addVehicle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String vehicleno = request.getParameter("vehicleno");
		String driverid = request.getParameter("driverid");
		int branchid = Integer.parseInt(request.getParameter("branchid"));

		VehicleService service = new VehicleService();
		Vehicle vehicle = new Vehicle();
		vehicle.setvehicleno(vehicleno);
		vehicle.setbranchid(branchid);
		vehicle.setdriverid(driverid);

		try {

			boolean result = service.addVehicle(vehicle);

			if (result) {
				request.setAttribute("message", "The vehicle is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-vehicle.jsp");
		rd.forward(request, response);

	}

	private void updateVehicle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String vehicleno = request.getParameter("vehicleno");
		int branchid = Integer.parseInt(request.getParameter("branchid"));
		String driverid = request.getParameter("driverid");

		Vehicle vehicle = new Vehicle(vehicleno, branchid, driverid);

		VehicleService service = new VehicleService();
		try {
			boolean result = service.updateVehicle(vehicle);
			if (result) {
				request.setAttribute("message",
						"The vehicle is updated successfully. vehicle_id:" + vehicle.getvehicleno());
			} else {
				request.setAttribute("message",
						"The vehicle is failed to updated! vehicle_id:" + vehicle.getvehicleno());
			}
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("vehicle_search-edit.jsp");
		rd.forward(request, response);

	}

	private void deleteVehicle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String vehicleno = request.getParameter("vehicleno");

		VehicleService service = new VehicleService();
		try {
			boolean result = service.deleteVehicle(vehicleno);
			if (result) {
				request.setAttribute("message", "The vehicle is deleted successfully. vehicle_id:" + vehicleno);
			} else {
				request.setAttribute("message", "The vehicle is failed to deleted! vehicle_id:" + vehicleno);
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("VehicleViewer?action=all");

	}

	private void addDriver(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String driverid = request.getParameter("driverid");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		int contact = Integer.parseInt(request.getParameter("contact"));
		VehicleService service = new VehicleService();
		Drivers drivers = new Drivers();
		drivers.setdriverid(driverid);
		drivers.setname(name);
		drivers.setaddress(address);
		drivers.setcontact(contact);
		try {
			boolean result = service.addDriver(drivers);

			if (result) {
				request.setAttribute("message", "The Driver is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-driver.jsp");
		rd.forward(request, response);

	}
}
