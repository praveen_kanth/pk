package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Customer;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.CustomerService;

public class CustomerManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			String action = request.getParameter("action");
			String action1 = request.getParameter("action1");
			if (action != null && action.equals("add")) {

				addCustomer(request, response);
			} else if (action != null && action.equals("edit")) {
				updateCustomer(request, response);
			} else if (action != null && action.equals("delete")) {
				deleteCustomer(request, response);
			} else if (action1 != null && action1.equals("r")) {
				BranchService service = new BranchService();
				try {

					List<Branch> branchList = service.getbranchs();
					request.setAttribute("branchList", branchList);
				} catch (ClassNotFoundException | SQLException e) {

					request.setAttribute("message", e.getMessage());
				}

				RequestDispatcher rd = request.getRequestDispatcher("add-customer.jsp");
				rd.forward(request, response);
			}

		} catch (Exception ws) {
			noAccess(request, response);
		}
	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");

		if (action != null && action.equals("edit")) {
			request.setAttribute("message", "No Access!");
			RequestDispatcher rd = request.getRequestDispatcher("customer_search-edit.jsp");
			rd.forward(request, response);

		} else {

			RequestDispatcher rd = request.getRequestDispatcher("add-customer.jsp");
			rd.forward(request, response);
		}

	}

	private void addCustomer(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String Name = request.getParameter("Name");
		String Email = request.getParameter("Email");
		int contact = Integer.parseInt(request.getParameter("contact"));
		String Nic = request.getParameter("Nic");
		int branchid = Integer.parseInt(request.getParameter("branchid"));
		CustomerService service = new CustomerService();
		Customer customer = new Customer();
		customer.setNic(Nic);
		customer.setName(Name);
		customer.setEmail(Email);
		customer.setcontact(contact);
		customer.setbranchid(branchid);
		try {

			boolean result = service.addCustomer(customer);

			if (result) {
				request.setAttribute("message", "The customer is added successfully!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("add-customer.jsp");
		rd.forward(request, response);

	}

	private void updateCustomer(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String Nic = request.getParameter("nic");
		int contact = Integer.parseInt(request.getParameter("contact"));
		String Name = request.getParameter("name");
		String Email = request.getParameter("email");
		int branchid = Integer.parseInt(request.getParameter("branchid"));

		Customer customer = new Customer(Nic, Name, Email, contact, branchid);

		CustomerService service = new CustomerService();
		try {
			boolean result = service.updatecustomer(customer);
			if (result) {
				request.setAttribute("message",
						"The customer is updated successfully. customer_id:" + customer.getNic());
			} else {
				request.setAttribute("message", "The customer is failed to updated! customer_id:" + customer.getNic());
			}
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("customer_search-edit.jsp");
		rd.forward(request, response);

	}

	private void deleteCustomer(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String Nic = request.getParameter("Nic");

		CustomerService service = new CustomerService();
		try {
			boolean result = service.deletecustomer(Nic);
			if (result) {
				request.setAttribute("message", "The customer is deleted successfully. customer_id: " + Nic);
			} else {
				request.setAttribute("message", "The customer is failed to deleted! customer_id: " + Nic);
			}

		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		response.sendRedirect("CustomerViewer?action=all");

	}
}
