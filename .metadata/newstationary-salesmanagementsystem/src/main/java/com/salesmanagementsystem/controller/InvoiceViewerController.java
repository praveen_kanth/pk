package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.salesmanagementsystem.model.Invoice;
import com.salesmanagementsystem.service.InvoiceService;

public class InvoiceViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			if (action != null && action.equals("all")) {
				getInvoiceList(request, response);
			} else {
				getInvoice(request, response);
			}
		}

		catch (Exception d) {
		}

	}

	private void getInvoiceList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		InvoiceService service = new InvoiceService();

		try {

			List<Invoice> invoiceList = service.getInvoices();
			request.setAttribute("invoiceList", invoiceList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("invoice-details.jsp");
		rd.forward(request, response);

	}

	public void getInvoice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int invoice_id = Integer.parseInt(request.getParameter("invoice_id"));

		InvoiceService service = new InvoiceService();

		try {
			Invoice invoice = service.getInvoice(invoice_id);
			request.setAttribute("invoice", invoice);
		}

		catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("invoice_search-edit.jsp");
		rd.forward(request, response);
	}
}
