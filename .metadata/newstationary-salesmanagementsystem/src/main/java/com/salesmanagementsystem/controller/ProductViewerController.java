package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salesmanagementsystem.model.Branch;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.model.StockReceived;
import com.salesmanagementsystem.model.StockRequest;
import com.salesmanagementsystem.model.StockReturn;
import com.salesmanagementsystem.model.Vehicle;
import com.salesmanagementsystem.service.BranchService;
import com.salesmanagementsystem.service.ProductService;
import com.salesmanagementsystem.service.StockRequestService;
import com.salesmanagementsystem.service.StockReturnService;
import com.salesmanagementsystem.service.VehicleService;

public class ProductViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			// session
			Employee employee = new Employee();
			HttpSession session = request.getSession();
			employee = (Employee) session.getAttribute("employee");
			if (employee.getPermission().equalsIgnoreCase("super")) {
				if (action != null && action.equals("all")) {
					getProductList(request, response);
				} else if (action != null && action.equals("stockrequest")) {
					getStockRequest(request, response);
				} else if (action != null && action.equals("stockreceived")) {
					getStockReceived(request, response);
				} else if (action != null && action.equals("stockreturn")) {
					getStockReturn(request, response);
				} else {
					getProduct(request, response);
				}
			} else if (action != null && action.equals("stockrequest")) {

				getStockRequest(request, response);
			} else if (action != null && action.equals("stockreceived")) {
				getStockReceived(request, response);
			} else if (action != null && action.equals("stockreturn")) {
				noAccess(request, response);
			} else {
				noAccess(request, response);
			}

		} catch (Exception a) {

			// noAccess(request,response);

		}
	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");

		if (action != null && action.equals("all")) {
			getProductList(request, response);
		} else if (action != null && action.equals("stockreturn")) {
			request.setAttribute("message", "No Access!");
			RequestDispatcher rd = request.getRequestDispatcher("stockreturndetails.jsp");
			rd.forward(request, response);
		} else {
			request.setAttribute("message", "No Access!");
			RequestDispatcher rd = request.getRequestDispatcher("search-edit.jsp");
			rd.forward(request, response);
		}

	}

	private void getProductList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductService service = new ProductService();

		try {
			System.out.println("2");
			List<Product> productList = service.getProducts();
			request.setAttribute("productList", productList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("product-details.jsp");

		rd.forward(request, response);

	}

	public void getProduct(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int product_id = Integer.parseInt(request.getParameter("product_id"));

		ProductService service = new ProductService();

		try {
			Product product = service.getProduct(product_id);
			request.setAttribute("product", product);
		}

		catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("search-edit.jsp");
		rd.forward(request, response);
	}
	
	/*
	 * private void getProductListByBranch(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException, IOException {
	 * 
	 * ProductService service = new ProductService();
	 * 
	 * try { Employee employee = new Employee(); HttpSession session =
	 * request.getSession(); employee=(Employee)session.getAttribute("employee");
	 * List<Product> productList = //
	 * service.getProductByBranch(employee.getbranch_id());
	 * request.setAttribute("productList", productList); } catch
	 * (ClassNotFoundException | SQLException e) {
	 * 
	 * request.setAttribute("message",e.getMessage()); }
	 * 
	 * RequestDispatcher rd = request.getRequestDispatcher("product-details.jsp");
	 * rd.forward(request, response);
	 * 
	 * }
	 */

	private void getStockRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StockRequestService service = new StockRequestService();
		BranchService service1 = new BranchService();
		VehicleService service2 = new VehicleService();
		try {

			List<StockRequest> stockrequestList = service.getStockRequests();
			List<Branch> branchList = service1.getbranchs();
			List<Vehicle> vehicleList = service2.getVehicles();
			request.setAttribute("branchList", branchList);
			request.setAttribute("stockrequestList", stockrequestList);
			request.setAttribute("vehicleList", vehicleList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("stockrequestdetails.jsp");
		rd.forward(request, response);

	}

	private void getStockReceived(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StockRequestService service = new StockRequestService();
		try {

			List<StockReceived> stockreceivedList = service.getStockReceives();
			request.setAttribute("stockreceivedList", stockreceivedList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("stockrecieveddetails.jsp");
		rd.forward(request, response);

	}

	private void getStockReturn(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StockReturnService service = new StockReturnService();

		try {

			List<StockReturn> stockreturnList = service.getStockReturns();
			request.setAttribute("stockreturnList", stockreturnList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("stockreturndetails.jsp");
		rd.forward(request, response);

	}
}
