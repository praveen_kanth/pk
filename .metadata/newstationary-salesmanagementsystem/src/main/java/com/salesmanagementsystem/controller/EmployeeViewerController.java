package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.service.EmployeeService;

public class EmployeeViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		Employee employee1 = new Employee();
		HttpSession session = request.getSession();
		employee1 = (Employee) session.getAttribute("employee");
		if (employee1.getPermission().equalsIgnoreCase("super")) {
			if (action != null && action.equals("all")) {
				getEmployeeList(request, response);
			} else {
				getEmployee(request, response);
			}
		} else {
			noAccess(request, response);
		}
	}

	private void noAccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("message", "No Access!");
		RequestDispatcher rd = request.getRequestDispatcher("add-employee.jsp");
		rd.forward(request, response);
	}

	private void getEmployeeList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		EmployeeService service = new EmployeeService();

		try {

			List<Employee> employeeList = service.getEmployees();
			request.setAttribute("employeeList", employeeList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("employeedetails.jsp");
		rd.forward(request, response);

	}

	public void getEmployee(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String employee_id = request.getParameter("employee_id");

		EmployeeService service = new EmployeeService();

		try {
			Employee employee = service.getEmployee(employee_id);
			request.setAttribute("employee", employee);
		}

		catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("emp_search-edit.jsp");
		rd.forward(request, response);
	}
}
