package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.salesmanagementsystem.model.Order;

import com.salesmanagementsystem.service.OrderService;

public class OrderViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String action = request.getParameter("action");

			if (action != null && action.equals("all")) {
				getOrderList(request, response);
			} else {
				getOrder(request, response);
			}
		} catch (Exception d) {
		}

	}

	private void getOrderList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		OrderService service = new OrderService();

		try {

			List<Order> OrderList = service.getOrders();
			request.setAttribute("OrderList", OrderList);
		} catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("#.jsp");
		rd.forward(request, response);

	}

	public void getOrder(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int Order_id = Integer.parseInt(request.getParameter("Order_id"));

		OrderService service = new OrderService();

		try {
			Order Order = service.getOrder(Order_id);
			request.setAttribute("Order", Order);
		}

		catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("message", e.getMessage());
		}

		RequestDispatcher rd = request.getRequestDispatcher("#.jsp");
		rd.forward(request, response);
	}
}
