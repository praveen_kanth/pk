package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Customer;

public class CustomerDao {
	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addcustomer(Customer customer) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		String query = "INSERT INTO customer (Nic,Name,Email,contact,branchid) VALUES (?,?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, customer.getNic());
		statement.setString(2, customer.getName());
		statement.setString(3, customer.getEmail());
		statement.setInt(4, customer.getcontact());
		statement.setInt(5, customer.getbranchid());
		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static Customer getcustomer(String nic) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM customer WHERE Nic = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, nic);

		ResultSet rs = statement.executeQuery();

		Customer customer = new Customer();

		while (rs.next()) {

			customer.setNic(rs.getString("Nic"));
			customer.setName(rs.getString("Name"));
			customer.setEmail(rs.getString("Email"));
			customer.setcontact(rs.getInt("contact"));
			customer.setbranchid(rs.getInt("branchid"));
		}

		statement.close();
		connection.close();

		return customer;
	}

	public static List<Customer> getcustomers() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM customer";

		ResultSet rs = statement.executeQuery(query);

		List<Customer> customerList = new ArrayList<Customer>();

		while (rs.next()) {

			Customer customer = new Customer();
			customer.setNic(rs.getString("Nic"));
			customer.setName(rs.getString("Name"));
			customer.setEmail(rs.getString("Email"));
			customer.setcontact(rs.getInt("contact"));
			customer.setbranchid(rs.getInt("branchid"));
			customerList.add(customer);

		}

		statement.close();
		connection.close();

		return customerList;
	}

	public static boolean updatecustomer(Customer customer) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "UPDATE customer SET Name = ?,Email = ?, contact = ? WHERE Nic = ? ";
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, customer.getName());
		statement.setString(2, customer.getEmail());
		statement.setInt(3, customer.getcontact());
		statement.setString(4, customer.getNic());
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean deletecustomer(String Nic) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM customer WHERE Nic = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, Nic);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

}
