package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Invoice;
import com.salesmanagementsystem.model.Order;

public class InvoiceDao {
	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addInvoice(Invoice invoice) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO invoice (invoiceid, order_id,invoicedate,amount,employee_id) VALUES (?,?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, invoice.getInvoiceid());
		statement.setInt(2, invoice.getOrderid());
		statement.setString(3, invoice.getDate());
		statement.setDouble(4, invoice.getAmount());
		statement.setString(5, invoice.getEmployeeid());
		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static Invoice getInvoice(int invoiceid) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM invoice WHERE invoiceid = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, invoiceid);

		ResultSet rs = statement.executeQuery();

		Invoice invoice = new Invoice();

		while (rs.next()) {

			invoice.setInvoiceid(rs.getInt("invoiceid"));
			invoice.setOrderid(rs.getInt("order_id"));
			invoice.setDate(rs.getString("invoicedate"));
			invoice.setAmount(rs.getDouble("amount"));
			invoice.setInvoiceid(rs.getInt("employee_id"));
		}

		statement.close();
		connection.close();

		return invoice;
	}

	public static List<Invoice> getInvoices() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM invoice";

		ResultSet rs = statement.executeQuery(query);
		System.out.println("ccc");
		List<Invoice> invoiceList = new ArrayList<Invoice>();

		while (rs.next()) {

			Invoice invoice = new Invoice();
			invoice.setInvoiceid(rs.getInt("invoiceid"));
			invoice.setOrderid(rs.getInt("order_id"));
			invoice.setDate(rs.getString("invoicedate"));
			invoice.setAmount(rs.getDouble("amount"));
			invoice.setInvoiceid(rs.getInt("employee_id"));
			invoiceList.add(invoice);

		}

		statement.close();
		connection.close();

		return invoiceList;
	}

	public static Invoice getInvoiceNo() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT max(invoiceid) as invoiceid FROM invoice";

		ResultSet rs = statement.executeQuery(query);
		rs.next();
		Invoice invoice = new Invoice();
		int lastval = 1;
		lastval = rs.getInt("invoiceid") + 1;
		invoice.setInvoiceid(lastval);

		statement.close();
		connection.close();

		return invoice;
	}

	public static boolean deleteInvoice(int invoiceid) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM invoice WHERE invoiceid = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setInt(1, invoiceid);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

}
