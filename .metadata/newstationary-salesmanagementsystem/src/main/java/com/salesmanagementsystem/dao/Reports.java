package com.salesmanagementsystem.dao;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.salesmanagementsystem.model.Employee;
import com.itextpdf.text.FontFactory;

public class Reports {
	private static DbUtils dbUtils = new MySQLConnector();

	public void execute() {

	}

	public void sales(Employee employee) throws ClassNotFoundException, SQLException, IOException {

		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("D:/Sales.pdf"));
			document.open();
			// Setting font of the text

			Font fontColour_Red = FontFactory.getFont(FontFactory.TIMES, 40f, Font.BOLD, BaseColor.RED);

			Paragraph paragraph1 = new Paragraph();
			paragraph1.setFont(fontColour_Red);

			paragraph1.setAlignment(Element.ALIGN_CENTER);
			paragraph1.add("Monthly Sales");

			document.add(paragraph1);

			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);

			PdfPTable table = new PdfPTable(9);
			table.addCell("Order Id");
			table.addCell("Nic");
			table.addCell("Order Date");
			table.addCell("Branch");
			table.addCell("Customer Name");
			table.addCell("Amount");
			table.addCell("Product");
			table.addCell("Quantity");
			table.addCell("Price");

			Connection connection = dbUtils.openConnection();
			String query ="";
			PreparedStatement statement;
			if (employee.getPermission().equalsIgnoreCase("super")) {
			 query = "SELECT * FROM customerorder";
			  statement = connection.prepareStatement(query);
			}
			else
			{
			query = "SELECT * FROM customerorder WHERE branchid = ?";
			statement = connection.prepareStatement(query);
			statement.setInt(1, employee.getbranch_id());			
			}
			

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				table.addCell(Integer.toString(rs.getInt("order_id")));
				table.addCell(rs.getString("nic"));
				table.addCell(rs.getString("orderdate"));
				table.addCell(Integer.toString(rs.getInt("branchid")));
				table.addCell(rs.getString("cus_name"));
				table.addCell(Double.toString(rs.getDouble("amount")));
				table.addCell(Integer.toString(rs.getInt("productid")));
				table.addCell(Integer.toString(rs.getInt("quantity")));
				table.addCell(Double.toString(rs.getDouble("price")));

			}
			document.add(table);
			document.close();
			Desktop.getDesktop().open(new File("D:/Sales.pdf"));

		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void purchase(Employee employee) throws ClassNotFoundException, SQLException, IOException {

		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("D:/Purchase.pdf"));
			document.open();
			// Setting font of the text

			Font fontColour_Red = FontFactory.getFont(FontFactory.TIMES, 30f, Font.BOLD, BaseColor.RED);

			Paragraph paragraph1 = new Paragraph();
			paragraph1.setFont(fontColour_Red);

			paragraph1.setAlignment(Element.ALIGN_CENTER);
			paragraph1.add("Monthly Stock Inbound/Outbound");

			document.add(paragraph1);

			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);

			PdfPTable table = new PdfPTable(5);
			table.addCell("Stock Request Id");
			table.addCell("Branch");
			table.addCell("Product Name");
			table.addCell("Quantity");
			table.addCell("Status");


			Connection connection = dbUtils.openConnection();
			String query = "SELECT * FROM stockrequest";
			PreparedStatement statement = connection.prepareStatement(query);

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				table.addCell(rs.getString("stockrequestid"));
				table.addCell(Integer.toString(rs.getInt("branch_id")));
				table.addCell(rs.getString("product_name"));
				table.addCell(Integer.toString(rs.getInt("product_quantity")));
				table.addCell(rs.getString("acknowledge"));
			
				

			}
			document.add(table);
			document.close();
			Desktop.getDesktop().open(new File("D:/Purchase.pdf"));

		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}