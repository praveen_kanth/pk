package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.salesmanagementsystem.model.StockReceived;
import com.salesmanagementsystem.model.StockRequest;

public class StockRequestDao {

	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addStockRequest(StockRequest Stockrequest) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO stockrequest (stockrequestid,product_id,product_name, product_quantity, branch_id, acknowledge) VALUES (?,?,?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, Stockrequest.getStockrequestid());
		statement.setInt(2, Stockrequest.getProduct_id());
		statement.setString(3, Stockrequest.getProduct_name());
		statement.setInt(4, Stockrequest.getProduct_quantity());
		statement.setInt(5, Stockrequest.getBranch_id());
		statement.setString(6, Stockrequest.getAcknowledge());

		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static StockRequest getStockRequest(int product_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM stockrequest WHERE product_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, product_id);

		ResultSet rs = statement.executeQuery();

		StockRequest stockrequest = new StockRequest();

		while (rs.next()) {

			stockrequest.setProduct_id(rs.getInt("product_id"));
			stockrequest.setProduct_name(rs.getString("product_name"));
			stockrequest.setProduct_quantity(rs.getInt("product_quantity"));
			stockrequest.setBranch_id(rs.getInt("branch_id"));
			stockrequest.setAcknowledge(rs.getString("acknowledge"));
		}

		statement.close();
		connection.close();

		return stockrequest;
	}

	public static List<StockRequest> getStockRequests() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM stockrequest";

		ResultSet rs = statement.executeQuery(query);

		List<StockRequest> stockrequestList = new ArrayList<StockRequest>();

		while (rs.next()) {

			StockRequest stockrequest = new StockRequest();
			stockrequest.setStockrequestid(rs.getString("stockrequestid"));
			stockrequest.setProduct_id(rs.getInt("product_id"));
			stockrequest.setProduct_name(rs.getString("product_name"));
			stockrequest.setProduct_quantity(rs.getInt("product_quantity"));
			stockrequest.setBranch_id(rs.getInt("branch_id"));
			stockrequest.setAcknowledge(rs.getString("acknowledge"));

			stockrequestList.add(stockrequest);

		}

		statement.close();
		connection.close();

		return stockrequestList;
	}

	public static List<StockReceived> getStockReceives() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM recievestock";

		ResultSet rs = statement.executeQuery(query);

		List<StockReceived> stockreceivedList = new ArrayList<StockReceived>();

		while (rs.next()) {

			StockReceived stockreceived = new StockReceived();
			stockreceived.setrequestid(rs.getString("requestid"));
			stockreceived.setproductid(rs.getInt("productid"));
			stockreceived.setproductquantity(rs.getInt("productquantity"));
			stockreceived.setbranch_id(rs.getInt("branch_id"));
			stockreceived.setfrombranchid(rs.getInt("frombranchid"));
			stockreceivedList.add(stockreceived);

		}

		statement.close();
		connection.close();
		return stockreceivedList;
	}

	public static boolean addStockReceived(StockReceived stockreceived) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO recievestock (requestid, productid, productquantity, branch_id, frombranchid) values (?,?,?,?,?)";
		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, stockreceived.getrequestid());
		statement.setInt(2, stockreceived.getproductid());
		statement.setInt(3, stockreceived.getproductquantity());
		statement.setInt(4, stockreceived.getbranch_id());
		statement.setInt(5, stockreceived.getfrombranchid());
		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean updateStockRequest(StockRequest Stockrequest) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		System.out.println("iop " + Stockrequest.getAcknowledge());
		String query = "UPDATE stockrequest SET stockrequestid = ?, product_id = ?, product_name = ?, branch_id = ?, product_quantity = ?,acknowledge = ? WHERE stockrequestid = ? ";
		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, Stockrequest.getStockrequestid());
		statement.setInt(2, Stockrequest.getProduct_id());
		statement.setString(3, Stockrequest.getProduct_name());
		statement.setInt(4, Stockrequest.getBranch_id());
		statement.setInt(5, Stockrequest.getProduct_quantity());
		statement.setString(6, Stockrequest.getAcknowledge());
		statement.setString(7, Stockrequest.getStockrequestid());
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean deleteStockRequest(int product_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM stockrequest WHERE product_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setInt(1, product_id);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean deleteStockReceived(String requestid) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM recievestock WHERE requestid = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, requestid);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static StockRequest getStockRequestNo() throws ClassNotFoundException, SQLException {
		StockRequest order = new StockRequest();
		try {
			Connection connection = dbUtils.openConnection();
			String query = "SELECT stockrequestid FROM sales_management_system.stockrequest\r\n"
					+ "ORDER BY stockrequestid DESC\r\n" + "LIMIT  1";

			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
			rs.next();
			String lastval = rs.getString("stockrequestid");
			int val1 = Integer.parseInt(lastval) + 1;
			String val2 = String.valueOf(val1);
			order.setStockrequestid(val2);
			statement.close();
			connection.close();

			return order;
		}

		catch (Exception s) {
			order.setStockrequestid("1000");
			return order;
		}

	}
}
