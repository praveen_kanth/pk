package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Employee;

public class EmployeeDao {

	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addEmployee(Employee employee) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO employee (employee_id, employee_firstname, employee_lastname, employee_type, employee_address, employee_email, employee_telephone, employee_password,permission,branch_id) VALUES (?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, employee.getEmployee_id());
		statement.setString(2, employee.getEmployee_firstname());
		statement.setString(3, employee.getEmployee_lastname());
		statement.setString(4, employee.getEmployee_type());
		statement.setString(5, employee.getEmployee_address());
		statement.setString(6, employee.getEmployee_email());
		statement.setInt(7, employee.getEmployee_telephone());
		statement.setString(8, employee.getEmployee_password());
		statement.setString(9, employee.getPermission());
		statement.setInt(10, employee.getbranch_id());

		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static Employee getEmployee(String employee_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM employee WHERE employee_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, employee_id);

		ResultSet rs = statement.executeQuery();

		Employee employee = new Employee();

		while (rs.next()) {

			employee.setEmployee_id(rs.getString("employee_id"));
			employee.setEmployee_firstname(rs.getString("employee_firstname"));
			employee.setEmployee_lastname(rs.getString("employee_lastname"));
			employee.setEmployee_type(rs.getString("employee_type"));
			employee.setEmployee_address(rs.getString("Employee_address"));
			employee.setEmployee_email(rs.getString("employee_email"));
			employee.setEmployee_telephone(rs.getInt("employee_telephone"));
			employee.setEmployee_password(rs.getString("employee_password"));
			employee.setPermission(rs.getString("permission"));
			employee.setbranch_id(rs.getInt("branch_id"));
		}

		statement.close();
		connection.close();

		return employee;
	}

	public static Employee loginEmployee(String employee_id, String password)
			throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		String val = "0";
		String query = "SELECT employee_id,employee_password,permission,branch_id,employee_email FROM employee WHERE employee_id = ? AND employee_password = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, employee_id);
		statement.setString(2, password);
		ResultSet rs = statement.executeQuery();

		Employee employee = new Employee();

		while (rs.next()) {

			employee.setEmployee_id(rs.getString("employee_id"));
			employee.setEmployee_password(rs.getString("employee_password"));
			employee.setPermission(rs.getString("permission"));
			employee.setbranch_id(rs.getInt("branch_id"));
			employee.setEmployee_email(rs.getString("employee_email"));
			val = "1";
		}
		statement.close();
		connection.close();
		if (val == "0") {
			employee = null;
		}
		return employee;
	}

	public static List<Employee> getEmployees() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM employee";

		ResultSet rs = statement.executeQuery(query);

		List<Employee> employeeList = new ArrayList<Employee>();

		while (rs.next()) {

			Employee employee = new Employee();
			employee.setEmployee_id(rs.getString("employee_id"));
			employee.setEmployee_firstname(rs.getString("employee_firstname"));
			employee.setEmployee_lastname(rs.getString("employee_lastname"));
			employee.setEmployee_type(rs.getString("employee_type"));
			employee.setEmployee_address(rs.getString("employee_address"));
			employee.setEmployee_email(rs.getString("employee_email"));
			employee.setEmployee_telephone(rs.getInt("employee_telephone"));
			employee.setEmployee_password(rs.getString("employee_password"));
			employee.setPermission(rs.getString("permission"));
			employee.setbranch_id(rs.getInt("branch_id"));

			employeeList.add(employee);

		}

		statement.close();
		connection.close();

		return employeeList;
	}

	public static boolean updateEmployee(Employee employee) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "UPDATE employee SET employee_firstname = ?, employee_lastname = ?, employee_type = ?, employee_address = ?, employee_email = ?, employee_telephone = ? WHERE employee_id = ? ";
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, employee.getEmployee_firstname());
		statement.setString(2, employee.getEmployee_lastname());
		statement.setString(3, employee.getEmployee_type());
		statement.setString(4, employee.getEmployee_address());
		statement.setString(5, employee.getEmployee_email());
		statement.setInt(6, employee.getEmployee_telephone());
		statement.setString(7, employee.getEmployee_id());
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean deleteEmployee(String employee_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM employee WHERE employee_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, employee_id);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

}
