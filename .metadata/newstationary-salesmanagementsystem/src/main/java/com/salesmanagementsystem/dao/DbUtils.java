package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface DbUtils {
	
	public Connection openConnection() throws ClassNotFoundException, SQLException;
	

}
