package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Branch;

public class BranchDao {
	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addbranch(Branch branch) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO branch (branch_id, branch_name,email) VALUES (?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, branch.getbranch_id());
		statement.setString(2, branch.getbranch_name());
		statement.setString(3, branch.getemail());
		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static Branch getbranch(int branch_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM branch WHERE branch_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, branch_id);

		ResultSet rs = statement.executeQuery();

		Branch branch = new Branch();

		while (rs.next()) {

			branch.setbranch_id(rs.getInt("branch_id"));
			branch.setbranch_name(rs.getString("branch_name"));
			branch.setemail(rs.getString("email"));
		}

		statement.close();
		connection.close();

		return branch;
	}

	public static List<Branch> getbranchs() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM branch";

		ResultSet rs = statement.executeQuery(query);

		List<Branch> branchList = new ArrayList<Branch>();

		while (rs.next()) {

			Branch branch = new Branch();
			branch.setbranch_id(rs.getInt("branch_id"));
			branch.setbranch_name(rs.getString("branch_name"));
			branch.setemail(rs.getString("email"));
			branchList.add(branch);

		}

		statement.close();
		connection.close();

		return branchList;
	}

	public static boolean updatebranch(Branch branch) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "UPDATE branch SET branch_name = ? WHERE branch_id = ? ";
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, branch.getbranch_name());
		statement.setInt(2, branch.getbranch_id());
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean deletebranch(int branch_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM branch WHERE branch_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setInt(1, branch_id);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

}
