package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.service.ProductService;

public class ProductDao {

	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addProduct(Product product) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO product (product_id,product_name, product_quantity, original_price, profit, selling_price,branch_id) VALUES (?,?,?,?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, product.getProduct_id());
		statement.setString(2, product.getProduct_name());
		statement.setInt(3, product.getProduct_quantity());
		statement.setDouble(4, product.getOriginal_price());
		statement.setDouble(5, product.getProfit());
		statement.setDouble(6, product.getSelling_price());
		statement.setInt(7, product.getbranch_id());

		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static Product getProduct(int product_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM product WHERE product_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, product_id);

		ResultSet rs = statement.executeQuery();

		Product product = new Product();

		while (rs.next()) {

			product.setProduct_id(rs.getInt("product_id"));
			product.setProduct_name(rs.getString("product_name"));
			product.setProduct_quantity(rs.getInt("product_quantity"));
			product.setOriginal_price(rs.getDouble("original_price"));
			product.setProfit(rs.getDouble("profit"));
			product.setSelling_price(rs.getDouble("selling_price"));
			product.setbranch_id(rs.getInt("branch_id"));
		}

		statement.close();
		connection.close();

		return product;
	}

	public static List<Product> getProducts() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM product";

		ResultSet rs = statement.executeQuery(query);

		List<Product> productList = new ArrayList<Product>();

		while (rs.next()) {

			Product product = new Product();
			product.setProduct_id(rs.getInt("product_id"));
			product.setProduct_name(rs.getString("product_name"));
			product.setProduct_quantity(rs.getInt("product_quantity"));
			product.setOriginal_price(rs.getDouble("original_price"));
			product.setProfit(rs.getDouble("profit"));
			product.setSelling_price(rs.getDouble("selling_price"));
			product.setbranch_id(rs.getInt("branch_id"));

			productList.add(product);

		}

		statement.close();
		connection.close();

		return productList;
	}
	
	public static List<Product> getProductListByBranch(Employee employee) throws ClassNotFoundException, SQLException
	{
		Connection connection = dbUtils.openConnection();
		String query = "SELECT * FROM product where branch_id = ? ";		
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, employee.getbranch_id());
		ResultSet rs = statement.executeQuery();
		List<Product> productList = new ArrayList<Product>();

		while (rs.next()) {

			Product product = new Product();
			product.setProduct_id(rs.getInt("product_id"));
			product.setProduct_name(rs.getString("product_name"));
			product.setProduct_quantity(rs.getInt("product_quantity"));
			product.setOriginal_price(rs.getDouble("original_price"));
			product.setProfit(rs.getDouble("profit"));
			product.setSelling_price(rs.getDouble("selling_price"));
			product.setbranch_id(rs.getInt("branch_id"));

			productList.add(product);

		}

		statement.close();
		connection.close();

		return productList;
	}

	public static boolean updateProduct(Product product) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		String query = "UPDATE product SET product_name = ?, product_quantity = ?, original_price = ?, profit = ?, selling_price = ? WHERE product_id = ? ";
		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, product.getProduct_name());
		statement.setInt(2, product.getProduct_quantity());
		statement.setDouble(3, product.getOriginal_price());
		statement.setDouble(4, product.getProfit());
		statement.setDouble(5, product.getSelling_price());
		statement.setInt(6, product.getProduct_id());

		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean updateProductStock(Product product) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		Connection connection1 = dbUtils.openConnection();

		ProductService ser = new ProductService();
		int quantity = ser.getProductByBranch(product.getProduct_id(), product.getbranch_id()).getProduct_quantity();
		String query = "UPDATE product SET product_quantity = ? WHERE product_id = ? AND branch_id = ?";
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, quantity + product.getProduct_quantity());
		statement.setInt(2, product.getProduct_id());
		statement.setInt(3, product.getbranch_id());

		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		// dispatch branch product flow
		int quantity1 = ser.getProductByBranch(product.getProduct_id(), product.getFrom_branch_id())
				.getProduct_quantity();
		PreparedStatement statement1 = connection1.prepareStatement(query);
		statement1.setInt(1, quantity1 - product.getProduct_quantity());
		statement1.setInt(2, product.getProduct_id());
		statement1.setInt(3, product.getFrom_branch_id());

		result = statement1.executeUpdate();
		statement1.close();
		connection1.close();
		return result > 0;
	}

	public static boolean updateProductReturn(Product product) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		ProductService ser = new ProductService();
		int quantity = ser.getProductByBranch(product.getProduct_id(), product.getbranch_id()).getProduct_quantity();
		String query = "UPDATE product SET product_quantity = ? WHERE product_id = ? AND branch_id = ?";
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, quantity - product.getProduct_quantity());
		statement.setInt(2, product.getProduct_id());
		statement.setInt(3, product.getbranch_id());

		int result = statement.executeUpdate();

		statement.close();
		connection.close();
		return result > 0;
	}

	public static boolean deleteProduct(int product_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM product WHERE product_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setInt(1, product_id);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static Product getProductByBranch(int product_id, int branch_id)
			throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		String query = "SELECT * FROM product WHERE  product_id = ? AND branch_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, product_id);
		statement.setInt(2, branch_id);

		ResultSet rs = statement.executeQuery();
		Product product = new Product();
		while (rs.next()) {

			product.setProduct_id(rs.getInt("product_id"));
			product.setProduct_name(rs.getString("product_name"));
			product.setProduct_quantity(rs.getInt("product_quantity"));
			product.setOriginal_price(rs.getDouble("original_price"));
			product.setProfit(rs.getDouble("profit"));
			product.setSelling_price(rs.getDouble("selling_price"));
		}

		statement.close();
		connection.close();
		return product;
	}

}
