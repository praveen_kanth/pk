package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Drivers;
import com.salesmanagementsystem.model.Vehicle;

public class VehicleDao {
	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addvehicle(Vehicle vehicle) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO vehicle (vehicleno, branchid, driverid) VALUES (?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, vehicle.getvehicleno());
		statement.setInt(2, vehicle.getbranchid());
		statement.setString(3, vehicle.getdriverid());
		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static Vehicle getvehicle(String vehicleno) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM vehicle WHERE vehicleno = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, vehicleno);

		ResultSet rs = statement.executeQuery();

		Vehicle vehicle = new Vehicle();

		while (rs.next()) {

			vehicle.setvehicleno(rs.getString("vehicleno"));
			vehicle.setbranchid(rs.getInt("branchid"));
			vehicle.setdriverid(rs.getString("driverid"));
		}

		statement.close();
		connection.close();

		return vehicle;
	}

	public static List<Vehicle> getvehicles() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM vehicle";

		ResultSet rs = statement.executeQuery(query);

		List<Vehicle> vehicleList = new ArrayList<Vehicle>();

		while (rs.next()) {

			Vehicle vehicle = new Vehicle();
			vehicle.setvehicleno(rs.getString("vehicleno"));
			vehicle.setbranchid(rs.getInt("branchid"));
			vehicle.setdriverid(rs.getString("driverid"));
			vehicleList.add(vehicle);

		}

		statement.close();
		connection.close();

		return vehicleList;
	}

	public static boolean updatevehicle(Vehicle vehicle) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "UPDATE vehicle SET branchid = ?,driverid = ? WHERE vehicleno = ? ";
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(3, vehicle.getvehicleno());
		statement.setString(2, vehicle.getdriverid());
		statement.setInt(1, vehicle.getbranchid());
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean deletevehicle(String vehicleno) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM vehicle WHERE vehicleno = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, vehicleno);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

	public static boolean adddriver(Drivers driver) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "INSERT INTO driver (driverid, name, address,contact) VALUES (?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setString(1, driver.getdriverid());
		statement.setString(2, driver.getname());
		statement.setString(3, driver.getaddress());
		statement.setInt(4, driver.getcontact());
		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static List<Drivers> getdrivers() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM driver";

		ResultSet rs = statement.executeQuery(query);

		List<Drivers> driverList = new ArrayList<Drivers>();

		while (rs.next()) {

			Drivers driver = new Drivers();
			driver.setdriverid(rs.getString("driverid"));
			driver.setname(rs.getString("name"));
			driver.setaddress(rs.getString("address"));
			driver.setcontact(rs.getInt("contact"));
			driverList.add(driver);

		}

		statement.close();
		connection.close();

		return driverList;
	}

}
