package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.salesmanagementsystem.model.Order;

public class OrderDao {
	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addOrder(Order Order) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		String query = "INSERT INTO customerorder (order_id,nic,orderdate,branchid,cus_name,amount,productid,quantity,price) VALUES (?,?,?,?,?,?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, Order.getOrderid());
		statement.setString(2, Order.getNic());
		statement.setString(3, Order.getDate());
		statement.setInt(4, Order.getBranchid());
		statement.setString(5, Order.getName());
		statement.setDouble(6, Order.getAmount());
		statement.setInt(7, Order.getProduct_id());
		statement.setInt(8, Order.getQuantity());
		statement.setDouble(9, Order.getPrice());

		int result = statement.executeUpdate();
		statement.close();
		connection.close();

		return result > 0;
	}

	public static Order getOrder(int Order_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM customerorder WHERE orderid = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, Order_id);

		ResultSet rs = statement.executeQuery();

		Order Order = new Order();

		while (rs.next()) {

			Order.setOrderid(rs.getInt("order_id"));
			Order.setNic(rs.getString("nic"));
			Order.setDate(rs.getString("orderdate"));
			Order.setBranchid(rs.getInt("branchid"));
			Order.setName(rs.getString("cus_name"));
			Order.setAmount(rs.getDouble("amount"));
			Order.setProduct_id(rs.getInt("productid"));
			Order.setQuantity(rs.getInt("quantity"));
			Order.setPrice(rs.getDouble("price"));
		}

		statement.close();
		connection.close();

		return Order;
	}

	public static Order getOrderNo() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT max(order_id) as order_id FROM customerorder";

		ResultSet rs = statement.executeQuery(query);
		rs.next();
		Order order = new Order();
		int lastval = 1000;
		lastval = rs.getInt("order_id") + 1;
		order.setOrderid(lastval);

		statement.close();
		connection.close();

		return order;
	}

	public static List<Order> getOrders() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM customerorder";

		ResultSet rs = statement.executeQuery(query);

		List<Order> OrderList = new ArrayList<Order>();

		while (rs.next()) {

			Order Order = new Order();
			Order.setOrderid(rs.getInt("order_id"));
			Order.setNic(rs.getString("nic"));
			Order.setDate(rs.getString("orderdate"));
			Order.setBranchid(rs.getInt("branchid"));
			Order.setName(rs.getString("cus_name"));
			Order.setAmount(rs.getDouble("amount"));
			Order.setProduct_id(rs.getInt("productid"));
			Order.setQuantity(rs.getInt("quantity"));
			Order.setPrice(rs.getDouble("price"));
			OrderList.add(Order);

		}

		statement.close();
		connection.close();

		return OrderList;
	}

	public static boolean deleteOrder(int Order_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "DELETE FROM customerorder WHERE order_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);

		statement.setInt(1, Order_id);
		int result = statement.executeUpdate();

		statement.close();
		connection.close();

		return result > 0;
	}

}
