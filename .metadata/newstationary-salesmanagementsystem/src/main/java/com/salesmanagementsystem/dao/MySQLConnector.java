package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnector implements DbUtils {

	@Override
	public Connection openConnection() throws ClassNotFoundException, SQLException {

		Class.forName("com.mysql.cj.jdbc.Driver");

		String user = "root";
		String password = "manager";
		String url = "jdbc:mysql://127.0.0.1:3306/sales_management_system";

		Connection connection = DriverManager.getConnection(url, user, password);

		return connection;
	}

}
