package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.salesmanagementsystem.model.Employee;

public class LogindDao {
	private static DbUtils dbUtils = new MySQLConnector();

	public Employee loginEmployee(String employee_id) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT employee_password FROM employee WHERE employee_id = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, employee_id);

		ResultSet rs = statement.executeQuery();

		Employee employee = new Employee();

		while (rs.next()) {

			employee.setEmployee_id("employee_id");
			employee.setEmployee_password(rs.getString("employee_password"));
		}

		statement.close();
		connection.close();

		return employee;
	}
}
