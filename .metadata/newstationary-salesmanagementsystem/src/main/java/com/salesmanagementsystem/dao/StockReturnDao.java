package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.model.StockReceived;
import com.salesmanagementsystem.model.StockRequest;
import com.salesmanagementsystem.model.StockReturn;
import com.salesmanagementsystem.service.ProductService;

public class StockReturnDao {

	private static DbUtils dbUtils = new MySQLConnector();

	public static boolean addStockReturn(StockReturn stockreturn) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();
		ProductService prod = new ProductService();

		String query = "INSERT INTO stockreturn (returnid,productid,branchid, productquantity) VALUES (?,?,?,?)";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, stockreturn.getreturnid());
		statement.setInt(2, stockreturn.getproductid());
		statement.setInt(3, stockreturn.getbranchid());
		statement.setInt(4, stockreturn.getproductquantity());

		Product proudct = new Product();
		proudct.setbranch_id(stockreturn.getbranchid());
		proudct.setProduct_id(stockreturn.getproductid());
		proudct.setProduct_quantity(stockreturn.getproductquantity());

		int result = statement.executeUpdate();
		statement.close();
		connection.close();
		prod.updateProductReturn(proudct);
		return result > 0;
	}

	public static StockReturn getStockReturn(int returnid) throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		String query = "SELECT * FROM stockreturn WHERE returnid = ?";

		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, returnid);

		ResultSet rs = statement.executeQuery();

		StockReturn stockreturn = new StockReturn();

		while (rs.next()) {

			stockreturn.setreturnid(rs.getString("returnid"));
			stockreturn.setproductid(rs.getInt("productid"));
			stockreturn.setproductquantity(rs.getInt("productquantity"));
			stockreturn.setbranchid(rs.getInt("branchid"));

		}

		statement.close();
		connection.close();

		return stockreturn;
	}

	public static List<StockReturn> getStockReturns() throws ClassNotFoundException, SQLException {

		Connection connection = dbUtils.openConnection();

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM stockreturn";

		ResultSet rs = statement.executeQuery(query);

		List<StockReturn> stockreturnList = new ArrayList<StockReturn>();

		while (rs.next()) {

			StockReturn stockreturn = new StockReturn();
			stockreturn.setreturnid(rs.getString("returnid"));
			stockreturn.setproductid(rs.getInt("productid"));
			stockreturn.setproductquantity(rs.getInt("productquantity"));
			stockreturn.setbranchid(rs.getInt("branchid"));

			stockreturnList.add(stockreturn);

		}

		statement.close();
		connection.close();

		return stockreturnList;
	}

	public static StockReturn getStockReturnNo() {
		StockReturn order = new StockReturn();
		try {

			Connection connection = dbUtils.openConnection();
			String query = "SELECT returnid FROM sales_management_system.stockreturn\r\n" + "ORDER BY returnid DESC\r\n"
					+ "LIMIT  1";

			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
			rs.next();
			String lastval = rs.getString("returnid");
			int val1 = Integer.parseInt(lastval) + 1;
			String val2 = String.valueOf(val1);
			order.setreturnid(val2);
			statement.close();
			connection.close();
			return order;
		} catch (Exception s) {
			order.setreturnid("1000");
			return order;
		}

	}

}
