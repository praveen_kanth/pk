package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.BranchDao;
import com.salesmanagementsystem.model.Branch;

public class BranchService {
	public boolean addBranch(Branch branch) throws ClassNotFoundException, SQLException {

		return BranchDao.addbranch(branch);

	}

	public Branch getbranch(int branch_id) throws ClassNotFoundException, SQLException {

		return BranchDao.getbranch(branch_id);

	}

	public List<Branch> getbranchs() throws ClassNotFoundException, SQLException {

		return BranchDao.getbranchs();
	}

	public boolean updatebranch(Branch branch) throws ClassNotFoundException, SQLException {

		return BranchDao.updatebranch(branch);
	}

	public boolean deletebranch(int branch_id) throws ClassNotFoundException, SQLException {

		return BranchDao.deletebranch(branch_id);
	}
}
