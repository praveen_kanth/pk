package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.ProductDao;
import com.salesmanagementsystem.model.Employee;
import com.salesmanagementsystem.model.Product;

public class ProductService {
	
	public boolean addProduct(Product product) throws ClassNotFoundException, SQLException {
		
		return ProductDao.addProduct(product);
		
	}

	public Product getProduct(int product_id) throws ClassNotFoundException, SQLException {
		
		return ProductDao.getProduct(product_id);
		
	}
	
	public List<Product> getProducts() throws ClassNotFoundException, SQLException{
		
		return ProductDao.getProducts();
	}
	public List<Product> getProductsByBranch(Employee employee) throws ClassNotFoundException, SQLException{
		
		return ProductDao.getProductListByBranch(employee);
	}
	
	public boolean updateProduct(Product product) throws ClassNotFoundException, SQLException {
		
		return ProductDao.updateProduct(product);
	}
	
	public boolean updateProductStock(Product product) throws ClassNotFoundException, SQLException {
		
		return ProductDao.updateProductStock(product);
	}
	
	public boolean updateProductReturn(Product product) throws ClassNotFoundException, SQLException {
		
		return ProductDao.updateProductReturn(product);
	}
	
	public boolean deleteProduct(int product_id) throws ClassNotFoundException, SQLException {
		
		return ProductDao.deleteProduct(product_id);
	}
	
	public Product getProductByBranch(int product_id,int branch_id) throws ClassNotFoundException, SQLException {
		
		return ProductDao.getProductByBranch(product_id,branch_id);
		
	}
}
