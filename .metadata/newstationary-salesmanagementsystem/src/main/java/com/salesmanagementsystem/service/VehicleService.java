package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.mysql.cj.jdbc.Driver;
import com.salesmanagementsystem.dao.VehicleDao;
import com.salesmanagementsystem.model.Drivers;
import com.salesmanagementsystem.model.Vehicle;

public class VehicleService {
public boolean addVehicle(Vehicle Vehicle) throws ClassNotFoundException, SQLException {
		
		return VehicleDao.addvehicle(Vehicle);
		
	}

	public Vehicle getVehicle(String Vehicleno) throws ClassNotFoundException, SQLException {
		
		return VehicleDao.getvehicle(Vehicleno);
		
		
	}
	
	public List<Vehicle> getVehicles() throws ClassNotFoundException, SQLException{
		
		return VehicleDao.getvehicles();
	}
	
	public boolean updateVehicle(Vehicle Vehicle) throws ClassNotFoundException, SQLException {
		
		return VehicleDao.updatevehicle(Vehicle);
	}
	
	public boolean deleteVehicle(String Vehicleno) throws ClassNotFoundException, SQLException {
		
		return VehicleDao.deletevehicle(Vehicleno);
	}
	
	public List<Drivers> getDrivers() throws ClassNotFoundException, SQLException{
		
		return VehicleDao.getdrivers();
	}
	
	public boolean addDriver(Drivers Driver) throws ClassNotFoundException, SQLException {
		
		return VehicleDao.adddriver(Driver);
		
	}
}
