package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.StockRequestDao;
import com.salesmanagementsystem.dao.StockReturnDao;
import com.salesmanagementsystem.model.StockReceived;
import com.salesmanagementsystem.model.StockRequest;
import com.salesmanagementsystem.model.StockReturn;


public class StockReturnService {
	
	public boolean addStockReturn(StockReturn stockreturn) throws ClassNotFoundException, SQLException {
		
		return StockReturnDao.addStockReturn(stockreturn);
		
	}

	public StockReturn getStockReturn(int StockRequest_id) throws ClassNotFoundException, SQLException {
		
		return StockReturnDao.getStockReturn(StockRequest_id);
		
	}
	
	public List<StockReturn> getStockReturns() throws ClassNotFoundException, SQLException{
		
		return StockReturnDao.getStockReturns();
	}
	
	public StockReturn getStockReturnNo() throws ClassNotFoundException, SQLException {
		
		return StockReturnDao.getStockReturnNo();
		
	}
}
