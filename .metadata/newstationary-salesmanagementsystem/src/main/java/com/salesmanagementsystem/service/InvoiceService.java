package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.InvoiceDao;
import com.salesmanagementsystem.dao.OrderDao;
import com.salesmanagementsystem.model.Invoice;
import com.salesmanagementsystem.model.Order;

public class InvoiceService {
public boolean addInvoice(Invoice invoice) throws ClassNotFoundException, SQLException {
		
		return InvoiceDao.addInvoice(invoice);
		
	}

	public Invoice getInvoice(int invoice_id) throws ClassNotFoundException, SQLException {
		
		return InvoiceDao.getInvoice(invoice_id);
		
	}
	
	public List<Invoice> getInvoices() throws ClassNotFoundException, SQLException{
		
		return InvoiceDao.getInvoices();
	}
	
public Invoice getInvoiceNo() throws ClassNotFoundException, SQLException {
		
		return InvoiceDao.getInvoiceNo();
		
	}
	
	public boolean deleteInvoice(int invoice_id) throws ClassNotFoundException, SQLException {
		
		return InvoiceDao.deleteInvoice(invoice_id);
	}
}
