package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.CustomerDao;
import com.salesmanagementsystem.model.Customer;

public class CustomerService {
public boolean addCustomer(Customer customer) throws ClassNotFoundException, SQLException {
		
		return CustomerDao.addcustomer(customer);
		
	}

	public Customer getcustomer(String Nic) throws ClassNotFoundException, SQLException {
		
		return CustomerDao.getcustomer(Nic);
		
	}
	
	public List<Customer> getcustomers() throws ClassNotFoundException, SQLException{
		
		return CustomerDao.getcustomers();
	}
	
	public boolean updatecustomer(Customer customer) throws ClassNotFoundException, SQLException {
		
		return CustomerDao.updatecustomer(customer);
	}
	
	public boolean deletecustomer(String Nic) throws ClassNotFoundException, SQLException {
		
		return CustomerDao.deletecustomer(Nic);
	}
}
