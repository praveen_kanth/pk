package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.EmployeeDao;
import com.salesmanagementsystem.model.Employee;

public class EmployeeService {
	
	public boolean addEmployee(Employee employee) throws ClassNotFoundException, SQLException {
		
		return EmployeeDao.addEmployee(employee);
		
	}

	public Employee getEmployee(String employee_id) throws ClassNotFoundException, SQLException {
		
		return EmployeeDao.getEmployee(employee_id);
		
	}
	
	public List<Employee> getEmployees() throws ClassNotFoundException, SQLException{
		
		return EmployeeDao.getEmployees();
	}
	
	public boolean updateEmployee(Employee employee) throws ClassNotFoundException, SQLException {
		
		return EmployeeDao.updateEmployee(employee);
	}
	
	public boolean deleteEmployee(String employee_id) throws ClassNotFoundException, SQLException {
		
		return EmployeeDao.deleteEmployee(employee_id);
	}
	
	public Employee loginEmployee(String employee_id, String password) throws ClassNotFoundException, SQLException {
		
		return EmployeeDao.loginEmployee(employee_id,password);
	}

}
