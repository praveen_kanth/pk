package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.OrderDao;
import com.salesmanagementsystem.model.Order;

public class OrderService {
public boolean addOrder(Order order) throws ClassNotFoundException, SQLException {
		
		return OrderDao.addOrder(order);
		
	}

	public Order getOrder(int Order_id ) throws ClassNotFoundException, SQLException {
		
		return OrderDao.getOrder(Order_id);
		
	}
	
	public Order getOrderNo() throws ClassNotFoundException, SQLException {
		
		return OrderDao.getOrderNo();
		
	}
	
	
	public List<Order> getOrders() throws ClassNotFoundException, SQLException{
		
		return OrderDao.getOrders();
	}
	
	
	public boolean deleteOrder(int Orderid) throws ClassNotFoundException, SQLException {
		
		return OrderDao.deleteOrder(Orderid);
	}
}
