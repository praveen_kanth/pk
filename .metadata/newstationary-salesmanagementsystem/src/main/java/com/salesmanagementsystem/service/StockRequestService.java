package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.OrderDao;
import com.salesmanagementsystem.dao.StockRequestDao;
import com.salesmanagementsystem.model.Order;
import com.salesmanagementsystem.model.StockReceived;
import com.salesmanagementsystem.model.StockRequest;


public class StockRequestService {
	
	public boolean addStockRequest(StockRequest stockrequest) throws ClassNotFoundException, SQLException {
		
		return StockRequestDao.addStockRequest(stockrequest);
		
	}

	public StockRequest getStockRequest(int StockRequest_id) throws ClassNotFoundException, SQLException {
		
		return StockRequestDao.getStockRequest(StockRequest_id);
		
	}
	
	public List<StockRequest> getStockRequests() throws ClassNotFoundException, SQLException{
		
		return StockRequestDao.getStockRequests();
	}
	
	public boolean updateStockRequest(StockRequest StockRequest) throws ClassNotFoundException, SQLException {
		
		return StockRequestDao.updateStockRequest(StockRequest);
	}
	
	public boolean deleteStockRequest(int StockRequest_id) throws ClassNotFoundException, SQLException {
		
		return StockRequestDao.deleteStockRequest(StockRequest_id);
	}
	
 public List<StockReceived> getStockReceives() throws ClassNotFoundException, SQLException{
		
		return StockRequestDao.getStockReceives();
	}
	
 public boolean addStockReceives(StockReceived stockreceived) throws ClassNotFoundException, SQLException {
		
		return StockRequestDao.addStockReceived(stockreceived);
		
	}
 
 public boolean deleteStockReceived(String StockRequest_id) throws ClassNotFoundException, SQLException {
		
		return StockRequestDao.deleteStockReceived(StockRequest_id);
	}
 
 public StockRequest getStockRequestNo() throws ClassNotFoundException, SQLException {
		
		return StockRequestDao.getStockRequestNo();
		
	}
	

}
