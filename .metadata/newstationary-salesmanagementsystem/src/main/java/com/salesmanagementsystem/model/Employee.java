package com.salesmanagementsystem.model;

public class Employee {

	private String employee_id;
	private String employee_firstname;
	private String employee_lastname;
	private String employee_type;
	private String employee_address;
	private String employee_email;
	private int employee_telephone;
	private String employee_password;
	private String permission;
	private int branch_id;

	public Employee() {
		
	}

	public Employee(String employee_id, String employee_firstname, String employee_lastname, String employee_type, String employee_address, String employee_email, int employee_telephone, String employee_password, String permission,int branchid) 
	{
		super();
		this.employee_id = employee_id;
		this.employee_firstname = employee_firstname;
		this.employee_lastname = employee_lastname;
		this.employee_type = employee_type;
		this.employee_address = employee_address;
		this.employee_email = employee_email;
		this.employee_telephone = employee_telephone;
		this.employee_password = employee_password;
		this.permission = permission;
		this.branch_id = branchid;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	
	public String getEmployee_firstname() {
		return employee_firstname;
	}

	public void setEmployee_firstname(String employee_firstname) {
		this.employee_firstname = employee_firstname;
	}
	
	public String getEmployee_lastname() {
		return employee_lastname;
	}

	public void setEmployee_lastname(String employee_lastname) {
		this.employee_lastname = employee_lastname;
	}


	public String getEmployee_type() {
		return employee_type;
	}
	
	public void setEmployee_type(String employee_type) {
		this.employee_type = employee_type;
	}
	
	public String getEmployee_address() {
		return employee_address;
	}
	
	public void setEmployee_address(String employee_address) {
		this.employee_address = employee_address;
	}
	
	public String getEmployee_email() {
		return employee_email;
	}
	
	public void setEmployee_email(String employee_email) {
		this.employee_email = employee_email;
	}
	
	public int getEmployee_telephone() {
		return employee_telephone;
	}
	
	public void setEmployee_telephone(int employee_telephone) {
		this.employee_telephone = employee_telephone;
	}
	
	public String getEmployee_password() {
		return employee_password;
	}

	public void setEmployee_password(String employee_password) {
		this.employee_password = employee_password;
	}
	
	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	public int getbranch_id() {
		return branch_id;
	}

	public void setbranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	
}
