package com.salesmanagementsystem.model;

public class Product {
	
	private int product_id;
	private String product_name;
	private int product_quantity;
	private double original_price;
	private double profit;
	private double selling_price;
	private int branch_id;
	private int from_branch_id;

	public Product() {
		
	}

	public Product(int product_id, String product_name, int product_quantity, double original_price, double profit,
			double selling_price,int branch_id) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_quantity = product_quantity;
		this.original_price = original_price;
		this.profit = profit;
		this.selling_price = selling_price;
		this.branch_id = branch_id;
	}
	
	public Product(int product_id, String product_name, int product_quantity) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_quantity = product_quantity;
		
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public int getProduct_quantity() {
		return product_quantity;
	}

	public void setProduct_quantity(int product_quantity) {
		this.product_quantity = product_quantity;
	}

	public double getOriginal_price() {
		return original_price;
	}

	public void setOriginal_price(double original_price) {
		this.original_price = original_price;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public double getSelling_price() {
		return selling_price;
	}

	public void setSelling_price(double selling_price) {
		this.selling_price = selling_price;
	}
	
	public int getbranch_id() {
		return branch_id;
	}

	public void setbranch_id(int branch_id) {
		this.branch_id = branch_id;
	}

	public int getFrom_branch_id() {
		return from_branch_id;
	}

	public void setFrom_branch_id(int from_branch_id) {
		this.from_branch_id = from_branch_id;
	}
	
}
