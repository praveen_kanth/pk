package com.salesmanagementsystem.model;

public class Drivers {
	
	private String driverid;
	private String name;
	private String address;
	private int contact;
	
	public Drivers() {
		
	}

	public Drivers(String driverid, String name,String address,int contact) {
		super();
		this.driverid = driverid;
		this.name = name;
		this.address = address;
		this.contact = contact;
	}

	public String getdriverid() {
		return driverid;
	}

	public void setdriverid(String driverid) {
		this.driverid = driverid;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}
	
	public String getaddress() {
		return address;
	}

	public void setaddress(String address) {
		this.address = address;
	}
	
	public int getcontact() {
		return contact;
	}
	public void setcontact(int contact) {
		this.contact = contact;
	}
}
