package com.salesmanagementsystem.model;

public class Branch {
	private int branch_id;
	private String branch_name;
	private String email;
	public Branch() {
		
	}

	public Branch(int branch_id, String branch_name,String email) {
		super();
		this.branch_id = branch_id;
		this.branch_name = branch_name;
		this.email = email;
	}
	public Branch(int branch_id, String branch_name) {
		super();
		this.branch_id = branch_id;
		this.branch_name = branch_name;
	}
	public int getbranch_id() {
		return branch_id;
	}

	public void setbranch_id(int branch_id) {
		this.branch_id = branch_id;
	}

	public String getbranch_name() {
		return branch_name;
	}

	public void setbranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	
	public String getemail() {
		return email;
	}

	public void setemail(String email) {
		this.email = email;
	}


}
