package com.salesmanagementsystem.model;

public class Customer {
	private String nic;
	private String name;
	private String email;
	private int contact;
	private int branchid;

	public Customer() {
		
	}
	
	public Customer(String Nic, String Name, String Email, int contact, int branchid) 
	{
		super();
		this.nic = Nic;
		this.name = Name;
		this.email = Email;
		this.contact = contact;
		this.branchid = branchid;		
	}
	
	public String getNic() {
		return nic;
	}

	public void setNic(String Nic) {
		this.nic = Nic;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String Name) {
		this.name = Name;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String Email) {
		this.email = Email;
	}
	public int getcontact() {
		return contact;
	}

	public void setcontact(int contact) {
		this.contact = contact;
	}
	public int getbranchid() {
		return branchid;
	}

	public void setbranchid(int branchid) {
		this.branchid = branchid;
	}
}
