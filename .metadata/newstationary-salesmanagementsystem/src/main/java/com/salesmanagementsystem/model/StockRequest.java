package com.salesmanagementsystem.model;

public class StockRequest {
	private String stockrequestid;
	private int product_id;
	private String product_name;
	private int product_quantity;
	private int branch_id;
	private String acknowledge;
	
	public StockRequest() {
		
	}
	
	public StockRequest(String stockrequestid,int product_id, String product_name, int product_quantity,int branch_id,String acknowledge) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_quantity = product_quantity;
		this.branch_id = branch_id;
		this.acknowledge = acknowledge;
		this.stockrequestid=stockrequestid;
	}
	
	public String getStockrequestid() {
		return stockrequestid;
	}

	public void setStockrequestid(String stockrequestid) {
		this.stockrequestid = stockrequestid;
	}
	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public int getProduct_quantity() {
		return product_quantity;
	}

	public void setProduct_quantity(int product_quantity) {
		this.product_quantity = product_quantity;
	}

	public int getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	
	public String getAcknowledge() {
		return acknowledge;
	}

	public void setAcknowledge(String acknowledge) {
		this.acknowledge = acknowledge;
	}
}
