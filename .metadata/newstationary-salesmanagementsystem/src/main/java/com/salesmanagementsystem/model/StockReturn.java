package com.salesmanagementsystem.model;

public class StockReturn {
	private String returnid;
	private int productid;
	private int productquantity;
	private int branchid;

	public StockReturn() {
		
	}
	
	public StockReturn(String returnid,int productid, int productquantity,int branchid) {
		super();
		this.returnid = returnid;
		this.productid = productid;
		this.productquantity = productquantity;
		this.branchid =branchid ;
	}
	
	public String getreturnid() {
		return returnid;
	}

	public void setreturnid(String returnid) {
		this.returnid = returnid;
	}
	public int getproductid() {
		return productid;
	}

	public void setproductid(int productid) {
		this.productid = productid;
	}
	public int getproductquantity() {
		return productquantity;
	}

	public void setproductquantity(int productquantity) {
		this.productquantity = productquantity;
	}

	public int getbranchid() {
		return branchid;
	}

	public void setbranchid(int branchid) {
		this.branchid = branchid;
	}
}
