package com.salesmanagementsystem.model;

public class StockReceived {
	private String requestid;
	private int productid;
	private int productquantity;
	private int branch_id;
	private int frombranchid;
	
	public StockReceived() {
		
	}
	
	public StockReceived(String requestid,int productid, int productquantity,int branch_id,int frombranchid) {
		super();
		this.requestid = requestid;
		this.productid = productid;
		this.productquantity = productquantity;
		this.branch_id = branch_id;
		this.frombranchid = frombranchid;
	}
	
	public String getrequestid() {
		return requestid;
	}

	public void setrequestid(String requestid) {
		this.requestid = requestid;
	}
	public int getproductid() {
		return productid;
	}

	public void setproductid(int productid) {
		this.productid = productid;
	}
	
	public int getproductquantity() {
		return productquantity;
	}

	public void setproductquantity(int productquantity) {
		this.productquantity = productquantity;
	}
	public int getbranch_id() {
		return branch_id;
	}

	public void setbranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public int getfrombranchid() {
		return frombranchid;
	}

	public void setfrombranchid(int frombranchid) {
		this.frombranchid = frombranchid;
	}
	
}
