package com.salesmanagementsystem.model;

public class Vehicle {
	private String vehicleno;
	private int branchid;
	private String driverid;
	public Vehicle() {
		
	}

	public Vehicle(String vehicleno, int branchid,String driverid) {
		super();
		this.vehicleno = vehicleno;
		this.branchid = branchid;
		this.driverid = driverid;
	}
	
	public int getbranchid() {
		return branchid;
	}

	public void setbranchid(int branchid) {
		this.branchid = branchid;
	}

	public String getvehicleno() {
		return vehicleno;
	}

	public void setvehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	
	public String getdriverid() {
		return driverid;
	}

	public void setdriverid(String driverid) {
		this.driverid = driverid;
	}


}
