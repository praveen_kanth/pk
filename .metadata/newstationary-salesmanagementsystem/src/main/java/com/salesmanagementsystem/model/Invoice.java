package com.salesmanagementsystem.model;

public class Invoice {
	
	private int invoiceid ; 
	private int orderid ; 
	private String employeeid;
	private String  date;
	private Double  amount;
	
	public Invoice() {
		
	}
	public Invoice(int invoiceid, int orderid,String employeeid,String date,Double amount) {
		super();
		this.invoiceid = invoiceid;
		this.orderid = orderid;
		this.employeeid = employeeid;
		this.date = date;
		this.amount = amount;
	}

	public int getInvoiceid() {
		return invoiceid;
	}

	public void setInvoiceid(int invoiceid) {
		this.invoiceid = invoiceid;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public String getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}