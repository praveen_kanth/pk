package com.salesmanagementsystem.model;

public class Order {
	
	private int orderid ; 
	private String nic;
	private int branchid;
	private String name;
	private double amount;
	private int product_id;
	private int quantity;
	private double price;
	private String  date;
	
	public Order() {
		
	}

	public Order(int orderid,  String Nic,int branchid,String Name,double Amount) {
		super();
		this.orderid = orderid;
		this.nic = Nic;
		this.branchid = branchid;
		this.name = Name;
		this.amount = Amount;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public int getBranchid() {
		return branchid;
	}

	public void setBranchid(int branchid) {
		this.branchid = branchid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
}
